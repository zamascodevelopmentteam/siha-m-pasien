package hi.studio.msiha_pasien

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.orhanobut.hawk.Hawk
import hi.studio.msiha_pasien.data.model.User
import hi.studio.msiha_pasien.di.AppComponent
import hi.studio.msiha_pasien.di.AppModule
import hi.studio.msiha_pasien.di.DaggerAppComponent
import timber.log.Timber

class App : Application() {

    companion object {
        private var appComponent: AppComponent? = null

        fun getComponent(): AppComponent {
            return appComponent!!
        }

        lateinit var instance: App
            private set

        private const val USER_KEY = "APP_USER_KEY"
        private const val TOKEN_KEY = "APP_TOKEN_KEY"
        private const val REFRESH_TOKEN_KEY = "APP_REFRESH_TOKEN_KEY"
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        appComponent = DaggerAppComponent.builder().appModule(AppModule(app = this)).build()
//        if (BuildConfig.DEBUG) {
//            Timber.plant(Timber.DebugTree())
//        }

        Hawk.init(this).build()
    }

    fun getToken(): String? = Hawk.get(TOKEN_KEY)

    fun setToken(token: String) = Hawk.put(TOKEN_KEY, token)

    fun getRefreshToken(): String? = Hawk.get(REFRESH_TOKEN_KEY)

    fun setRefreshToken(refreshToken: String) = Hawk.put(REFRESH_TOKEN_KEY, refreshToken)

    fun logout() {
        Hawk.delete(TOKEN_KEY)
        Hawk.delete(USER_KEY)
        Hawk.delete(REFRESH_TOKEN_KEY)
    }

    fun isLogin(): Boolean = getUser() != null

    fun setUser(user: User) = Hawk.put(USER_KEY, user)

    fun getUser(): User? {
        return Hawk.get<User?>(USER_KEY)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}
