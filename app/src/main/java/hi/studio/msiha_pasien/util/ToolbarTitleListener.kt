package hi.studio.msiha_pasien.util

import android.graphics.drawable.Drawable
import android.view.View

interface ToolbarTitleListener {
    // Using default value, to prevent assigning value on the implementation side
    fun updateTitle(title: String, subTitle: String = "", animate: Boolean = false)
    fun toolbarAction(onClickListener: View.OnClickListener)
    fun updateNavIcon(drawable: Drawable)
    fun showToolbar(show: Boolean)
}