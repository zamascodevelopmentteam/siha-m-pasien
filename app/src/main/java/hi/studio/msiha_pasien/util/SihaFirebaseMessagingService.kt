package hi.studio.msiha_pasien.util

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.orhanobut.hawk.Hawk
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.ui.main.MainActivity
import hi.studio.msiha_pasien.util.AppsContant.KEY_FCM_TOKEN
import timber.log.Timber

class SihaFirebaseMessagingService : FirebaseMessagingService() {

    var TAG = "FIREBASE MESSAGING"
    var nid=0
    private var channelID = "hipe_channel"

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        val mNotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        Log.d(TAG, "From: " + remoteMessage.from!!)

        if (remoteMessage.data.isNotEmpty()) {
            val type = remoteMessage.data["type"].toString()
            nid++
            val builder = generateNotification(remoteMessage)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                builder.setChannelId(channelID)
                builder.setVibrate(longArrayOf(1000, 1000))

                val attributes = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build()
                val mChannel = NotificationChannel(channelID,type,NotificationManager.IMPORTANCE_HIGH)
                mChannel.description=remoteMessage.data["description"].toString()
                mChannel.enableVibration(true)
                mChannel.enableLights(true)
                mChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
                mNotificationManager.createNotificationChannel(mChannel)
            }
            val notification = NotificationCompat.InboxStyle(builder)
            mNotificationManager.notify(nid, notification.build())
        }

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {

            Log.d(TAG, "Message Notification Body: " + remoteMessage.notification!!.body!!)
        }

    }

    private fun generateNotification(remoteMessage:RemoteMessage):NotificationCompat.Builder{
        val content = remoteMessage.data["content"].toString()
        val type =remoteMessage.data["type"].toString()
        val title =remoteMessage.data["title"].toString()

        return NotificationCompat.Builder(this,type)
            .setSmallIcon(R.drawable.ic_person)
            .setContentTitle(title)
            .setContentText(content)
            .setAutoCancel(true)
            .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
            .setStyle(NotificationCompat.BigTextStyle().bigText(content))
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setContentIntent(generatePendingIntent(remoteMessage))
    }

    private fun generatePendingIntent(remoteMessage:RemoteMessage):PendingIntent{
        val notificationIntent = Intent(applicationContext, MainActivity::class.java)
        notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        return PendingIntent.getActivity(
            applicationContext, 0,
            notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Log.d("fcm","new token $s")
        Hawk.put(KEY_FCM_TOKEN,s)
    }
}