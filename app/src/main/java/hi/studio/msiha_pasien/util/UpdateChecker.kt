package hi.studio.msiha_pasien.util

import android.app.Activity
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog

import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import hi.studio.msiha_pasien.BuildConfig

import timber.log.Timber

class UpdateChecker private constructor(
    private val activity: Activity
) {

    companion object {
        private const val UPDATE_URL = "patient_play_store_url"
        private const val NEWEST_APP_VERSION = "patient_newest_app_version"
        private const val NEWEST_APP_NAME = "patient_newest_app_name"
        private const val MIN_VERSION_FORCE_UPDATE = "patient_min_version_force_update"
        private const val ENABLE_FORCE_UPDATE = "patient_enable_force_update"
        private const val CACHE_EXPIRED: Long = 3600 //24 hours
        private const val FETCH_INTERVAL: Long = 3600 //24 Hours

        fun init(activity: Activity): UpdateChecker {
            return UpdateChecker(activity)
        }

        val appVersion: Float
            get() = BuildConfig.VERSION_CODE.toFloat()
    }

    private var callback: Callback? = null
    private var dialog: AlertDialog? = null

    fun setCallback(callback: Callback): UpdateChecker {
        this.callback = callback
        return this
    }

    fun check() {
        checkNotNull(callback) { "You must add callback" }
        callback?.onStartUpdateCheck()
        if (BuildConfig.DEBUG) {
            Timber.d("Check Update Debug Mode")
        } else {
            Timber.d("Check Update Release")
        }
        val frc = FirebaseRemoteConfig.getInstance()
        val builder = FirebaseRemoteConfigSettings.Builder()
            .setMinimumFetchIntervalInSeconds(FETCH_INTERVAL)
            .build()
        frc.setConfigSettingsAsync(builder)
        frc.fetch(CACHE_EXPIRED)
            .addOnCompleteListener(activity) { task ->
                if (task.isSuccessful) {
                    Timber.d("fetch remote config success")
                    frc.activate()
                } else {
                    Timber.d("fetch remote config failed")
                }
                fetchData()
            }
            .addOnFailureListener {
                callback?.onStopUpdateCheck()
                callback?.onCheckUpdateError(it.fillInStackTrace())
            }
    }

    private fun fetchData() {
        val rc = FirebaseRemoteConfig.getInstance()
        val currentAppVersionCode = appVersion
        val updateUrl = rc.getString(UPDATE_URL)

        val newestName: String
        val newestVersion: Float
        val minVersionForceUpdate: Float
        val enableForceUpdate: Boolean

        newestName = rc.getString(NEWEST_APP_NAME)
        newestVersion = rc.getDouble(NEWEST_APP_VERSION).toFloat()
        minVersionForceUpdate = rc.getDouble(MIN_VERSION_FORCE_UPDATE).toFloat()
        enableForceUpdate = rc.getBoolean(ENABLE_FORCE_UPDATE)

        Timber.d("current app version: $currentAppVersionCode")
        Timber.d("newest version: $newestVersion")
        Timber.d("enable force update: $enableForceUpdate")
        Timber.d("store url: $updateUrl")

        callback?.onStopUpdateCheck()

        if (!BuildConfig.DEBUG) {
            show(
                updateUrl,
                newestName,
                currentAppVersionCode,
                newestVersion,
                minVersionForceUpdate,
                enableForceUpdate
            )
        }
    }

    private fun show(
        updateUrl: String,
        newestAppName: String,
        currentAppVersionCode: Float,
        newestVersion: Float,
        minVersion: Float,
        isForceUpdate: Boolean
    ) {
        when {
            currentAppVersionCode < minVersion -> {
                Timber.d("version < minVersion, should update!")
                showDialog(true, updateUrl, newestAppName)
            }
            currentAppVersionCode < newestVersion -> {
                Timber.d("version < newVersion, ask to update")
                showDialog(isForceUpdate, updateUrl, newestAppName)
            }
            else -> {
                Timber.d("no update")
                callback?.onNoUpdate()
            }
        }
    }

    private fun showDialog(isForce: Boolean, updateUrl: String, appVersionName: String) {
        if (dialog == null)
            dialog = AlertDialog.Builder(activity).setTitle("Ada Versi Baru")
                .setMessage("Versi Terbaru Telah Tersedia! Segera perbarui aplikasi anda ke versi $appVersionName")
                .setCancelable(false)
                .setPositiveButton("Perbarui") { _, _ ->
                    callback?.onUpdatePressed(updateUrl)
                }
                .create()
        if (!isForce) {
            dialog?.setButton(DialogInterface.BUTTON_NEGATIVE, "Lewati") { dialogInterface, _ ->
                dialogInterface.dismiss()
                callback?.onSkipUpdatePressed()
            }
        }
        if (dialog!!.isShowing) {
            dialog?.dismiss()
        }
        dialog?.show()
    }

    interface Callback {
        fun onStartUpdateCheck()

        fun onStopUpdateCheck()

        fun onUpdatePressed(url: String)

        fun onSkipUpdatePressed()

        fun onNoUpdate()

        fun onCheckUpdateError(e: Throwable)
    }
}
