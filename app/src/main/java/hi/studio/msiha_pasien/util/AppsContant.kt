package hi.studio.msiha_pasien.util

object AppsContant {
    const val BUNDLE_KEY = "bundle_key"
    const val PREVIOUS_VISIT_KEY = "PREVIOUS_VISIT_KEY"
    //const val BASE_URL   = "http://167.71.197.186:9000/"
    const val BASE_URL   = "https://api.siha.zamasco.co.id/"
    const val KEY_FCM_TOKEN = "key_fcm_token"
    //const val BASE_URL = "http://api.m-siha.hi-studio.co/"
    //const val BASE_URL = "http://192.168.100.46:2017/"
    //const val BASE_URL = "https://8b259819.ngrok.io"
}