package hi.studio.msiha_pasien.util

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha_pasien.util.extension.orZero

abstract class EndlessScrollListener : RecyclerView.OnScrollListener() {

    private var previousTotal = 0
    private var isLoading = true

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val visibleItemCount = recyclerView.childCount
        val totalItemCount = recyclerView.layoutManager?.itemCount.orZero()
        val firsVisibleItem =
            (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

        if (isLoading && totalItemCount > previousTotal) {
            isLoading = false
            previousTotal = totalItemCount
        }

        if (!isLoading && (totalItemCount - visibleItemCount) <= (firsVisibleItem + VISIBLE_THRESHOLD)) {
            loadMore()
            isLoading = true
        }
    }

    public abstract fun loadMore()

    companion object {
        const val VISIBLE_THRESHOLD = 5
    }
}