package hi.studio.msiha_pasien.util.extension

import android.graphics.Bitmap
import android.graphics.Color
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import java.util.*
import org.joda.time.format.ISODateTimeFormat
import org.joda.time.DateTime
import java.text.SimpleDateFormat


fun Int?.orInvalidId(): Int = this ?: -1

fun Int?.orZero(): Int = this ?: 0

fun Boolean?.orFalse(): Boolean = this ?: false

@Throws(WriterException::class)
fun String.textToImageEncode(): Bitmap? {
    val bitMatrix: BitMatrix
    try {
        bitMatrix = MultiFormatWriter().encode(this, BarcodeFormat.QR_CODE, 500, 500, null)
    } catch (exception: IllegalArgumentException) {
        return null
    }

    val bitMatrixWidth = bitMatrix.width
    val bitMatrixHeight = bitMatrix.height
    val pixels = IntArray(bitMatrixWidth * bitMatrixHeight)

    for (y in 0 until bitMatrixHeight) {
        val offset = y * bitMatrixWidth
        for (x in 0 until bitMatrixWidth) {
            pixels[offset + x] = if (bitMatrix.get(x, y)) Color.BLACK else Color.WHITE
        }
    }
    val bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444)
    bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight)
    return bitmap
}

fun Long.toStringDateTime(): String {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    val year = calendar.get(Calendar.YEAR)
    val month = calendar.get(Calendar.MONTH)
    val date = calendar.get(Calendar.DAY_OF_MONTH)
    val day = calendar.get(Calendar.DAY_OF_WEEK)
    val hour = calendar.get(Calendar.HOUR_OF_DAY)
    val minute = calendar.get(Calendar.MINUTE)

    val days = listOf<String>("Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu")
    val months = listOf<String>(
        "Januari",
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember"
    )

    val strHour = if (hour < 10) "0$hour" else hour.toString()
    val strMinute = if (minute < 10) "0$minute" else minute.toString()

    return "${days[day % 7]}, ${date} ${months[month % 12]} $year, $strHour:$strMinute"
}

fun Long.toStringDate(): String {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    val year = calendar.get(Calendar.YEAR)
    val month = calendar.get(Calendar.MONTH)
    val date = calendar.get(Calendar.DAY_OF_MONTH)
    val day = calendar.get(Calendar.DAY_OF_WEEK)

    val days = listOf<String>("Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu")
    val months = listOf<String>(
        "Januari",
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember"
    )
    return "${days[day % 7]}, ${date} ${months[month % 12]} $year"
}

fun Long.toStringTime(): String {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    val hour = calendar.get(Calendar.HOUR_OF_DAY)
    val minute = calendar.get(Calendar.MINUTE)

    val strHour = if (hour < 10) "0$hour" else hour.toString()
    val strMinute = if (minute < 10) "0$minute" else minute.toString()

    return "$strHour:$strMinute"
}

fun String.toDatetime(): String {
    val utc = TimeZone.getTimeZone("UTC")
    val sourceFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    val destFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    sourceFormat.setTimeZone(utc)
    val convertedDate = sourceFormat.parse(this)
    return destFormat.format(convertedDate)
}

fun String.toDate(): String {
    val utc = TimeZone.getTimeZone("UTC")
    val sourceFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    val destFormat = SimpleDateFormat("yyyy-MM-dd")
    sourceFormat.timeZone = utc
    val convertedDate = sourceFormat.parse(this)
    return destFormat.format(convertedDate)
}

fun String.toReadableDate():String{
    val utc = TimeZone.getTimeZone("UTC")
    val sourceFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    val destFormat = SimpleDateFormat("dd/MM/yyyy")
    sourceFormat.timeZone = utc
    val convertedDate = sourceFormat.parse(this)
    return destFormat.format(convertedDate)
}

