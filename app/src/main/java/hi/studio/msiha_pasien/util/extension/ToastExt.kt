package hi.studio.msiha_pasien.util.extension

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

/**
 *
 * In syaa Allah created or modified by @mochadwi
 * On 02/04/19 for HipeAndroid
 */

/**
 * Ez toast
 *
 * Usage e.g:
 *
 * 1. Plain ways, repetitive (makeText, with this context, message and length)
 * Toast.makeText(this, "", Toast.LENGTH_LONG)
 *
 * 2. Alternative with kotlin extensions (Available on AppCompatActivity & Fragment)
 * toast("your message here")
 *
 * mToast declared as member to prevent leak memory, so we can configure it
 * E.g: toastSpammable, mToast.cancel() invoked
 * */
lateinit var mToast: Toast
var mToastLength = Toast.LENGTH_SHORT

fun AppCompatActivity.toast(msg: String, isLong: Boolean = false) {
    mToastLength =
        if (isLong) Toast.LENGTH_LONG
        else Toast.LENGTH_SHORT

    mToast = Toast.makeText(applicationContext, msg, mToastLength)
    mToast.show()

    if (isDestroyed) mToast.cancel()
}

fun AppCompatActivity.toastSpammable(msg: String, isLong: Boolean = false) {
    mToastLength =
        if (isLong) Toast.LENGTH_LONG
        else Toast.LENGTH_SHORT

    if (::mToast.isInitialized) mToast.cancel()
    mToast = Toast.makeText(applicationContext, msg, mToastLength)
    mToast.show()

    if (isDestroyed) mToast.cancel()
}

fun AppCompatActivity.toastLong(msg: String) {
    toast(msg, true)
}


fun AppCompatActivity.toastLongSpammable(msg: String) {
    toastSpammable(msg, true)
}

fun Fragment.toast(msg: String?) {
    msg?.let {
        (requireActivity() as? AppCompatActivity)?.toast(it)
    }
}

fun Fragment.toastLong(msg: String) {
    (requireActivity() as AppCompatActivity).toastLong(msg)
}

fun Fragment.toastSpammable(msg: String) {
    (requireActivity() as? AppCompatActivity)?.toastSpammable(msg)
}

fun Fragment.toastLongSpammable(msg: String) {
    (requireActivity() as AppCompatActivity).toastLongSpammable(msg)
}