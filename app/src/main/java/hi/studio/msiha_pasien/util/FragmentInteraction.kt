package hi.studio.msiha_pasien.util

interface FragmentInteraction{
    /***
     * return true to consume back press event
     * return false to let parent parent activity consume back press event
     */
    fun onBackPressed() : Boolean
}