package hi.studio.msiha_pasien.util.extension

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.load(url: String?) {
    url?.let {
        Glide.with(this).load(it).into(this)
    }
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}
