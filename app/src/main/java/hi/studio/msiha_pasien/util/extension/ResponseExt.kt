package hi.studio.msiha_pasien.util.extension

import com.google.gson.GsonBuilder
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response
import timber.log.Timber

fun Response<*>.getError(): Throwable {
    val errorBody = errorBody()?.string() ?: ""
    return Throwable(
        try {
            val json = JSONObject(errorBody)
            json.getString("message")
        } catch (e: Throwable) {
            Timber.e(e)
            errorBody
        }
    )
}

/**
 * https://futurestud.io/tutorials/retrofit-2-simple-error-handling
 */
fun <T> Response<Resource<T>>.parse(callback: (resource: Resource<T>) -> Unit) {
    if (isSuccessful) {
        //Error code from 200 - 299
        body()?.let {
            callback(Resource.success(it.data))
        } ?: kotlin.run {
            callback(Resource.error(this.message()))
        }
    } else {
        //Error code from 400 - 599
        Timber.d("Parse Error Server")
        Timber.d("error code: ${code()}")
        Timber.d("error message: ${message()}")
        when (code()) {
            404 -> callback(Resource.error("Data tidak ditemukan"))
            else -> {
                val gson = GsonBuilder().create()
                val resource = gson.fromJson(errorBody()?.string(), Resource::class.java)
                callback(Resource.error(resource.message.orEmpty()))
            }
        }
    }
}