package hi.studio.msiha_pasien.ui.main.dashboard.recipe

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hi.studio.msiha_pasien.data.model.ListRecipes
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.data.repo.RecipeRepo
import javax.inject.Inject

class RecipeListViewModel @Inject constructor(private val repo: RecipeRepo) : ViewModel() {

    val recipeResults =
        MutableLiveData<Resource<List<ListRecipes>>>().apply { loadRecipes(0) }

    fun loadRecipes(page: Int) {
        repo.getAllRecipes(page) { recipeResults.postValue(it) }
    }
}