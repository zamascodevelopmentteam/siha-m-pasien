package hi.studio.msiha_pasien.ui.main.dashboard.visit

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.data.model.MedicineGiven
import kotlinx.android.synthetic.main.item_medicine_remain.view.*

class MedicineRemainAdapter(private val interaction: Interaction) :
    RecyclerView.Adapter<MedicineRemainAdapter.MedicineRemainHolder>() {

    var data: List<MedicineGiven> = listOf()
    private val itemLayout = R.layout.item_medicine_remain

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedicineRemainHolder {
        val inflater = LayoutInflater.from(parent.context)
        return MedicineRemainHolder(inflater.inflate(itemLayout, parent, false))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: MedicineRemainHolder, position: Int) {
        holder.bind(data[position], interaction)
    }

    fun swapData(data: List<MedicineGiven>) {
        this.data = data
        notifyDataSetChanged()
    }

    class MedicineRemainHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: MedicineGiven, interaction: Interaction) =
            with(itemView) {
                itemMedicineRemainTextName.text = item.medicine.codeName
                itemMedicineRemainTextCounter.text = item.amount.toString()

                itemMedicineRemainButtonDecrement.setOnClickListener {
                    interaction.onItemDecrement(item)
                }

                itemMedicineRemainButtonIncrement.setOnClickListener {
                    interaction.onItemIncrement(item)
                }
            }
    }

    interface Interaction {
        fun onItemDecrement(item: MedicineGiven)
        fun onItemIncrement(item: MedicineGiven)
    }
}