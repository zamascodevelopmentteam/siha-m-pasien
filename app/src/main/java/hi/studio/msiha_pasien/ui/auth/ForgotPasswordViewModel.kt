package hi.studio.msiha_pasien.ui.auth

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.data.repo.UserRepo
import hi.studio.msiha_pasien.data.request.ResetPasswordRequest
import hi.studio.msiha_pasien.util.toSingleEvent
import javax.inject.Inject

class ForgotPasswordViewModel @Inject constructor(private val repo: UserRepo) : ViewModel() {

    val resetPasswordResult = MutableLiveData<Resource<Void>>().toSingleEvent()

    fun resetPassword(nik: String) {
        resetPasswordResult.postValue(Resource.loading())
        repo.resetPassword(ResetPasswordRequest(nik)) {
            resetPasswordResult.postValue(it)
        }
    }
}