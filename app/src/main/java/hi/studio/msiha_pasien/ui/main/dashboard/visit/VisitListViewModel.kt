package hi.studio.msiha_pasien.ui.main.dashboard.visit

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hi.studio.msiha_pasien.data.model.VisitByType
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.data.repo.VisitRepo
import hi.studio.msiha_pasien.util.toSingleEvent
import javax.inject.Inject

class VisitListViewModel @Inject constructor(private val repo: VisitRepo) : ViewModel() {

    val getVisitByTypeResult =
        MutableLiveData<Resource<List<VisitByType>>>().toSingleEvent()

    fun getVisitByType(page: Int) {
        repo.getVisitByType(page) {
            getVisitByTypeResult.postValue(it)
        }
    }
}