package hi.studio.msiha_pasien.ui.main.medicine

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hi.studio.msiha_pasien.data.model.RecipeGiven
import hi.studio.msiha_pasien.data.model.Treatment
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.data.repo.RecipeRepo
import hi.studio.msiha_pasien.util.toSingleEvent
import javax.inject.Inject

class MedicineListViewModel @Inject constructor(private val repo: RecipeRepo) : ViewModel() {

    val getRecipesGivenResult =
        MutableLiveData<Resource<List<RecipeGiven>>>().toSingleEvent().apply {
            getRecipesGiven(0)
        }

    val getUpdateSisaObat = MutableLiveData<Resource<Void>>().toSingleEvent()


    fun getRecipesGiven(page: Int) {
        //repo.getRecipesGiven(page) { getRecipesGivenResult.postValue(it) }
    }

    fun updateSisaObat(visitId:String,treatment: Treatment){
        repo.updateSisaObat(visitId,treatment){getUpdateSisaObat.postValue(it)}
    }
}