package hi.studio.msiha_pasien.ui.main.dashboard


import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.id.hipe.view.textdrawable.ColorGenerator
import com.id.hipe.view.textdrawable.TextDrawable
import com.orhanobut.hawk.Hawk
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.data.model.VisitByType
import hi.studio.msiha_pasien.data.network.wrapper.Status
import hi.studio.msiha_pasien.ui.common.DashboardContentAdapter
import hi.studio.msiha_pasien.ui.common.DashboardContentModel
import hi.studio.msiha_pasien.ui.main.dashboard.visit.VisitListFragment
import hi.studio.msiha_pasien.ui.main.dashboard.recipe.RecipeListFragment
import hi.studio.msiha_pasien.ui.main.dashboard.schedule.ScheduleListFragment
import hi.studio.msiha_pasien.util.AppsContant
import hi.studio.msiha_pasien.util.EndlessScrollListener
import hi.studio.msiha_pasien.util.extension.*
import kotlinx.android.synthetic.main.content_empty_list.*
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.fragment_dashboard.contentRecycler

class DashboardFragment : Fragment(),
    DashboardContentAdapter.Interaction  {

    private var nextPage=0
    private val arrayVisit = ArrayList<VisitByType>()

    private val dashboardContentAdapter by lazy {
        DashboardContentAdapter(this)
    }
    private val viewModel by lazy {
        createViewModel { App.getComponent().getVisitListViewModel() }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getVisitByType(nextPage)
        populateUserInfo()
        dashboardQrCodeFab.setOnClickListener {
            findNavController().navigate(R.id.qrStartFragment)
        }

        contentRecycler.apply {
            adapter = dashboardContentAdapter
            layoutManager = LinearLayoutManager(context)
            addOnScrollListener(object : EndlessScrollListener() {
                override fun loadMore() {
                    viewModel.getVisitByType(nextPage)
                }
            })
        }
        setupEmptyListMessage()
        observeViewModel()
    }

    private fun populateUserInfo(){
        val user = App.instance.getUser()
        if (user != null) {
            textPasienNik.text=user.nik
            textPasienName.text=user.name
            val defaultAvatar = TextDrawable.builder().buildRound(
                "" + user.name, ColorGenerator.MATERIAL.getColor(user.name)
            )
            textPasienStatus.text="Status: Pasien ${user.patient.statusPatient}"
            Glide.with(this)
                .asBitmap()
                .load(user.avatar)
                .apply(RequestOptions().circleCrop().error(defaultAvatar).placeholder(defaultAvatar))
                .into(imgPasienAvatar)
            Log.d("testing","user avatar link : ${user.avatar}")
        }
    }

    @SuppressLint("SetTextI18n")
    private fun observeViewModel() {
        viewModel.getVisitByTypeResult.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    Log.d("asuarab","size :${it.data?.size}")
                    if (it.data?.isNotEmpty().orFalse()) {
                        contentRecycler.visibility = View.VISIBLE
                        emptyState.visibility = View.INVISIBLE
                        nextPage++
                        var contentModels = it.data
                        contentModels?.sortedByDescending {it1->it1.ordinal}

                        val contents = ArrayList<DashboardContentModel>()
                        //val contents = mutableListOf<DashboardContentModel>().apply {
                            for ((i, item) in contentModels.orEmpty().withIndex()) {
                                val content= DashboardContentModel(
                                    item.id,
                                    "Kunjungan Ke-${item.ordinal}",
                                    try {
                                        item.visitDate.toDatetime()
                                    }catch(e:Exception){
                                        item.visitDate
                                    }
                                )
                                contents.add(content)
                                arrayVisit.add(item)

                                if(item.treatment!=null) {
                                    if (item.treatment.tglRencanaKunjungan == null) {
                                        textNextVisit.visibility=View.GONE
                                    } else {
                                        textNextVisit.visibility=View.VISIBLE
                                        textNextVisit.text =
                                            "Tanggal kunjungan selanjutnya : ${item.treatment.tglRencanaKunjungan.toReadableDate()}"
                                    }
                                }
                                if(i==0){
                                    Hawk.put("current_visit_id", item.id)
                                }
                                if(i==1) {
                                    Hawk.put("current_visit", item)
                                }
                                Log.d("test","visit id : ${item.id} - ordinal ${item.ordinal}")
                            }
                            Log.d("test","hawk : ${Hawk.get<Int>("current_visit_id")}")
                            dashboardContentAdapter.addData(contents)
                            if(it.data?.size==arrayVisit.size && nextPage>1){
                                Log.d("test","delete current visit $nextPage")
                                Hawk.delete("current_visit")
                                textNextVisit.text=""
                            }
                        //}


                        //Log.d("visit","contents list size : ${contents.size}")
                    }else{
                        contentRecycler.visibility = View.INVISIBLE
                        emptyState.visibility = View.VISIBLE
                    }
                    Log.d("visit","visit list size : ${dashboardContentAdapter.itemCount}")
                    if (dashboardContentAdapter.itemCount > 0) {
                        contentRecycler.visibility = View.VISIBLE
                        emptyState.visibility = View.INVISIBLE
                    } else {
                        contentRecycler.visibility = View.INVISIBLE
                        emptyState.visibility = View.VISIBLE
                    }
                }
                Status.ERROR -> toast(it.message)
                Status.LOADING -> {
                }
            }
        })
    }


    private fun setupEmptyListMessage() {
        imgEmpty.setImageDrawable(context?.let {
            ContextCompat.getDrawable(
                it,
                R.drawable.ic_empty_visit
            )
        })
        textEmptyMessage.text = "Tidak ada data kunjungan untuk ditampilkan"
    }

    override fun onItemClick(item: DashboardContentModel) {
       //Log.d("arrayVisit","array visit size : ${arrayVisit.size}")
        val bundle = Bundle()
        val idx = arrayVisit.indexOfFirst {
            it.id==item.id
        }
        Log.d("idx","idx ${idx}")
        if(idx!=-1 && idx<(arrayVisit.size-1)){
            bundle.putParcelable(AppsContant.BUNDLE_KEY,arrayVisit[idx])
            bundle.putParcelable(AppsContant.PREVIOUS_VISIT_KEY,arrayVisit[idx+1])
        }else if(idx!=-1 && idx>=(arrayVisit.size-1)){
            bundle.putParcelable(AppsContant.BUNDLE_KEY,arrayVisit[idx])
        }
        findNavController().navigate(R.id.diagnosisFragment, bundle)
    }
}
