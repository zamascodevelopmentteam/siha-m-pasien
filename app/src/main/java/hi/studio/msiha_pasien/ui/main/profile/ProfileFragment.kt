package hi.studio.msiha_pasien.ui.main.profile


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.data.model.User
import hi.studio.msiha_pasien.data.network.wrapper.Status
import hi.studio.msiha_pasien.ui.auth.AuthActivity
import hi.studio.msiha_pasien.util.extension.createViewModel
import hi.studio.msiha_pasien.util.extension.load
import hi.studio.msiha_pasien.util.extension.toast
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : Fragment() {

    private lateinit var viewModel: ProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.getComponent().inject(this)
        viewModel = createViewModel { App.getComponent().getProfileViewModel() }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        profileTextLogout.setOnClickListener { viewModel.logout() }
    }

    override fun onResume() {
        super.onResume()
        viewModel.userResult.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> setUserView(it.data)
                Status.ERROR -> toast(it.message)
                Status.LOADING -> {
                }
            }
        })

        viewModel.logoutResult.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    startActivity(Intent(context, AuthActivity::class.java))
                    activity?.finish()
                }
                Status.ERROR -> toast(it.message)
                Status.LOADING -> { }
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun setUserView(user: User?) {
        user?.let {
            profileAvatar.load(it.avatar)
            profileName.text = it.name
            profileNik.text = "NIK: ${it.nik}"
            //profileAddress.text = "Domisili: ${it.}"
        }
    }
}
