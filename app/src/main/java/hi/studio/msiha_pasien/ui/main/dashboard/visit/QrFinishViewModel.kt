package hi.studio.msiha_pasien.ui.main.dashboard.visit

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hi.studio.msiha_pasien.data.model.RecipeGivenStatus
import hi.studio.msiha_pasien.data.model.VisitStatus
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.data.repo.RecipeRepo
import hi.studio.msiha_pasien.data.repo.VisitRepo
import hi.studio.msiha_pasien.util.toSingleEvent
import javax.inject.Inject

class QrFinishViewModel @Inject constructor(private val repo: VisitRepo) : ViewModel() {

    val getVisitStatusResult =
        MutableLiveData<Resource<VisitStatus>>().toSingleEvent()

    fun getVisitStatus() {
        repo.getVisitStatus { getVisitStatusResult.postValue(it) }
    }
}