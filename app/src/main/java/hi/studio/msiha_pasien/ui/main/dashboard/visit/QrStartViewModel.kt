package hi.studio.msiha_pasien.ui.main.dashboard.visit

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hi.studio.msiha_pasien.data.model.RecipeGivenStatus
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.data.repo.RecipeRepo
import hi.studio.msiha_pasien.util.toSingleEvent
import javax.inject.Inject

class QrStartViewModel @Inject constructor(private val recipeRepo: RecipeRepo) : ViewModel() {

    val recipeGivenStatusResult =
        MutableLiveData<Resource<RecipeGivenStatus>>().toSingleEvent()

    fun getRecipeGivenStatus() {
        recipeRepo.getRecipeGivenStatus { recipeGivenStatusResult.postValue(it) }
    }
}