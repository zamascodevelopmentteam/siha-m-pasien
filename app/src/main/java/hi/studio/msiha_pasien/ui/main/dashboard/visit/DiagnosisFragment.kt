package hi.studio.msiha_pasien.ui.main.dashboard.visit


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.data.model.Hospital
import hi.studio.msiha_pasien.data.model.VisitByType
import hi.studio.msiha_pasien.data.network.wrapper.Status
import hi.studio.msiha_pasien.ui.common.DashboardContentAdapter
import hi.studio.msiha_pasien.ui.common.DashboardContentModel
import hi.studio.msiha_pasien.util.AppsContant
import hi.studio.msiha_pasien.util.extension.*
import kotlinx.android.synthetic.main.content_schedule.*
import kotlinx.android.synthetic.main.fragment_diagnosis.*

class DiagnosisFragment : Fragment(), DashboardContentAdapter.Interaction {

    private var visit: VisitByType? = null
    private var previousVisit: VisitByType? = null

    private val viewModel by lazy {
        createViewModel { App.getComponent().getDiagnosisViewModel() }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.getComponent().inject(this)
        visit = arguments?.getParcelable<VisitByType>(AppsContant.BUNDLE_KEY)
        previousVisit = arguments?.getParcelable<VisitByType>(AppsContant.PREVIOUS_VISIT_KEY)
        Log.d("visit","visit id ${visit?.id}")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_diagnosis, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("visit","visit ${visit?.treatment}")
        showDiagnosisResult(visit)
        showSisaObat(previousVisit)
        //viewModel.getPreviousVisit(visitId = visit?.id?:0)
    }

    private fun observeViewMode(){
        viewModel.getPreviousVisitDetailResult.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    previousVisit=it.data

                }
                Status.ERROR -> toast(it.message)
                Status.LOADING -> {
                }
            }
        })
    }


    private fun showSisaObat(visitByType: VisitByType?){
        visitByType?.run{
            if(this.treatment!=null) {
                if (this.treatment.obatArv1Data != null) {
                    sisaObatArv1Name.text = this.treatment.obatArv1Data.codeName
                    val count1 = this.treatment.sisaObatArv1 ?: "0"
                    sisaObatArv1Sum.text = "Sisa Obat Sebelumnya : $count1"
                }
                if (this.treatment.obatArv2Data != null) {
                    val count2 = this.treatment.sisaObatArv1 ?: "0"
                    sisaObatArv2Name.text = this.treatment.obatArv2Data?.codeName
                    sisaObatArv2Sum.text = "Sisa Obat Sebelumnya : $count2"
                }else{
                    wrapperArv2.visibility=View.GONE
                    separatorArv2.visibility=View.INVISIBLE
                }
                if (this.treatment.obatArv3Data != null) {
                    val count3 = this.treatment.sisaObatArv1 ?: "0"
                    sisaObatArv3Name.text = this.treatment.obatArv3Data.codeName
                    sisaObatArv3Sum.text = "Sisa Obat Sebelumnya : $count3"
                }else{
                    wrapperArv3.visibility=View.GONE
                    separatorArv3.visibility=View.INVISIBLE
                }
            }else{
                wrapperSisaArv1.visibility=View.GONE
                wrapperSisaArv2.visibility=View.GONE
                wrapperSisaArv3.visibility=View.GONE
                separatorSisaArv1.visibility=View.INVISIBLE
                separatorSisaArv2.visibility=View.INVISIBLE
            }
        }
    }

    private fun showDiagnosisResult(visitByType: VisitByType?) {
        visitByType?.run {
            textKunjunganOrdinal.text = "${this.ordinal}"
            textTempatKunjungan.text = this.upk?.name
            textWaktuKunjungan.text = this.visitDate.toDatetime()
            if(this.treatment!=null) {
                if (this.treatment.obatArv1Data != null) {
                    obatArv1Name.text = this.treatment.obatArv1Data.codeName
                    val count1 = this.treatment.jmlHrObatArv1 ?: "0"
                    obatArv1Sum.text = "Jumlah : $count1"
                }
                if (this.treatment.obatArv2Data != null) {
                    val count2 = this.treatment.jmlHrObatArv2 ?: "0"
                    obatArv2Name.text = this.treatment.obatArv2Data?.codeName
                    obatArv2Sum.text = "Jumlah : $count2"
                }else{
                    wrapperArv2.visibility=View.GONE
                    separatorArv2.visibility=View.INVISIBLE
                }
                if (this.treatment.obatArv3Data != null) {
                    val count3 = this.treatment.jmlHrObatArv3 ?: "0"
                    obatArv3Name.text = this.treatment.obatArv3Data.codeName
                    obatArv3Sum.text = "Jumlah : $count3"
                }else{
                    wrapperArv3.visibility=View.GONE
                    separatorArv3.visibility=View.INVISIBLE
                }
            }else{
                separatorArv1.visibility=View.INVISIBLE
                separatorArv2.visibility=View.INVISIBLE
                separatorArv3.visibility=View.INVISIBLE
            }
        }
    }

    override fun onItemClick(item: DashboardContentModel) {

    }
}
