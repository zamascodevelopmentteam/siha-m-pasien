package hi.studio.msiha_pasien.ui.main.dashboard.schedule


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.data.model.Hospital
import hi.studio.msiha_pasien.data.model.VisitSchedule
import hi.studio.msiha_pasien.data.network.wrapper.Status
import hi.studio.msiha_pasien.util.AppsContant
import hi.studio.msiha_pasien.util.extension.createViewModel
import hi.studio.msiha_pasien.util.extension.toStringDate
import hi.studio.msiha_pasien.util.extension.toStringTime
import hi.studio.msiha_pasien.util.extension.toast
import kotlinx.android.synthetic.main.content_schedule.*
import kotlinx.android.synthetic.main.fragment_schedule.*

class ScheduleDetailFragment : Fragment() {

    private var visitSchedule: VisitSchedule? = null

    private val viewModel by lazy {
        createViewModel { App.getComponent().getScheduleDetailViewModel() }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.getComponent().inject(this)
        visitSchedule = arguments?.getParcelable(AppsContant.BUNDLE_KEY)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_schedule, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showVisitDetailResult(visitSchedule)
        visitSchedule?.let { viewModel.getHospitalById(it.hospitalId) }

        scheduleButtonRescheduleRequest.setOnClickListener {
            findNavController().navigate(R.id.rescheduleFragment)
        }

        observeViewModel()
    }

    private fun showVisitDetailResult(visitSchedule: VisitSchedule?) {
        visitSchedule?.run {
//            diagnosisTextDate.text = visitDate.toStringDate()
//            diagnosisTextTime.text = visitDate.toStringTime()
//            diagnosisType.text = "Anti-$visitType"
//            diagnosisTextVisitCounter.text = visitOrdinal.toString()
            scheduleTextDoctor.text = doctorName
        }
    }

    private fun observeViewModel() {
        viewModel.getHospitalByIdResult.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> showHospitalResult(it.data)
                Status.ERROR -> toast(it.message)
                Status.LOADING -> {
                }
            }
        })
    }

    private fun showHospitalResult(hospital: Hospital?) {
        hospital?.run {
           // diagnosisTextPlace.text = name
        }
    }
}
