package hi.studio.msiha_pasien.ui.main.dashboard.recipe


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.data.model.Recipe
import hi.studio.msiha_pasien.data.model.RecipeMedicine
import hi.studio.msiha_pasien.data.network.wrapper.Status
import hi.studio.msiha_pasien.ui.common.RecipeMedicineAdapter
import hi.studio.msiha_pasien.util.AppsContant
import hi.studio.msiha_pasien.util.extension.createViewModel
import hi.studio.msiha_pasien.util.extension.orInvalidId
import hi.studio.msiha_pasien.util.extension.toast
import kotlinx.android.synthetic.main.content_recipe_detail.*
import kotlinx.android.synthetic.main.fragment_recipe.*

class RecipeDetailFragment : Fragment() {

    private val recipeMedicineAdapter by lazy {
        RecipeMedicineAdapter()
    }
    private val viewModel by lazy {
        createViewModel { App.getComponent().getDetailRecipeViewModel() }
    }
    private val detailRecipeId: Int by lazy {
        arguments?.getInt(AppsContant.BUNDLE_KEY).orInvalidId()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.getComponent().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_recipe, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recipeRecycler.apply {
            adapter = recipeMedicineAdapter
            layoutManager = LinearLayoutManager(context)
        }

        viewModel.getDetailRecipe(detailRecipeId)
        viewModel.detailRecipeResult.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> showRecipeDetail(it.data)
                Status.ERROR -> toast(it.message)
                Status.LOADING -> {}
            }
        })
    }

    private fun showRecipeDetail(recipe: Recipe?) {
        recipe?.run {
            recipeTextDoctorNote.text = notes
            recipeTextNumber.text = recipeNumber
            showRecipeRecycler(recipeMedicines.orEmpty())
        }
    }

    private fun showRecipeRecycler(recipeMedicines: List<RecipeMedicine>) {
        recipeMedicineAdapter.swapData(recipeMedicines)
        recipeRecycler.apply {
            adapter = recipeMedicineAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }
}
