package hi.studio.msiha_pasien.ui.main.medicine


import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.data.model.Recipe
import hi.studio.msiha_pasien.data.model.RecipeMedicine
import hi.studio.msiha_pasien.data.network.wrapper.Status
import hi.studio.msiha_pasien.ui.common.RecipeMedicineAdapter
import hi.studio.msiha_pasien.util.extension.*
import kotlinx.android.synthetic.main.content_recipe_detail.*
import kotlinx.android.synthetic.main.fragment_medicine_detail.*

class MedicineDetailFragment : Fragment() {

    private val recipeMedicineAdapter by lazy {
        RecipeMedicineAdapter()
    }
    private val detailRecipeId: Int by lazy {
        arguments?.getInt("recipeId").orInvalidId()
    }
    private val hospitalName: String by lazy {
        arguments?.getString("hospitalName").orEmpty()
    }
    private val hospitalCity: String by lazy {
        arguments?.getString("hospitalCity").orEmpty()
    }
    private val viewModel by lazy {
        createViewModel { App.getComponent().getMedicineDetailViewModel() }
    }

    private var cancelTask = false
    private lateinit var handler: Handler
    private lateinit var statusChecker: Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.getComponent().inject(this)
        handler = Handler()
        statusChecker = Runnable {
            if (!cancelTask) {
                handler.postDelayed({
                    viewModel.getRecipeGivenStatus()
                    statusChecker.run()
                }, 1000L)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_medicine_detail, container, false)
    }

    override fun onPause() {
        super.onPause()
        cancelTask = true
        handler.removeCallbacks(statusChecker)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        medicineDetailTextClinic.text = hospitalName
        medicineDetailTextCity.text = hospitalCity

        showQrCode()

        recipeRecycler.apply {
            adapter = recipeMedicineAdapter
            layoutManager = LinearLayoutManager(context)
        }
        viewModel.getDetailRecipe(detailRecipeId)

        observeViewModel()
        statusChecker.run()
    }

    private fun observeViewModel() {
        viewModel.run {
            detailRecipeResult.observe(this@MedicineDetailFragment, Observer {
                when (it.status) {
                    Status.SUCCESS -> showRecipeDetail(it.data)
                    Status.ERROR -> toast(it.message)
                    Status.LOADING -> {
                    }
                }
            })
            recipeGivenStatusResult.observe(this@MedicineDetailFragment, Observer {
                when (it.status) {
                    Status.SUCCESS -> {
                        if (it.data?.isRecipeGiven.orFalse()) {
                            findNavController().navigate(R.id.medicineFinishFragment)
                        }
                    }
                    Status.ERROR -> {
                    }
                    Status.LOADING -> {
                    }
                }
            })
        }

    }

    private fun showRecipeDetail(recipe: Recipe?) {
        recipe?.run {
            recipeTextNumber.text = recipeNumber
            showRecipeRecycler(recipeMedicines.orEmpty())
        }
    }

    private fun showRecipeRecycler(recipeMedicines: List<RecipeMedicine>) {
        recipeMedicineAdapter.swapData(recipeMedicines)
        recipeRecycler.apply {
            adapter = recipeMedicineAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    private fun showQrCode() {
        val user = App.instance.getUser()
        if (user == null) {
            toast("Tidak dapat menampilkan QR Code")
        } else {
            val image = user.id.toString().textToImageEncode()
            medicineDetailImageQrCode.setImageBitmap(image)
        }
    }
}
