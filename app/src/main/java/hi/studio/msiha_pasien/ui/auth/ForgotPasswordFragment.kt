package hi.studio.msiha_pasien.ui.auth


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.util.extension.createViewModel
import kotlinx.android.synthetic.main.content_forgot_password_input.*
import kotlinx.android.synthetic.main.content_forgot_password_send.*
import kotlinx.android.synthetic.main.fragment_forgot_password.*

class ForgotPasswordFragment : Fragment() {

    private lateinit var viewModel: ForgotPasswordViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.getComponent().inject(this)
        viewModel = createViewModel { App.getComponent().getForgotPasswordViewModel() }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_forgot_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as? AppCompatActivity)?.supportActionBar?.show()
//
//        forgotPasswordButtonSearch.setOnClickListener {
//            forgotPasswordInput.visibility = View.GONE
//            forgotPasswordSend.visibility = View.VISIBLE
//        }
//
//        forgotPasswordButtonSend.setOnClickListener {
//            findNavController().navigateUp()
//        }
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.resetPasswordResult.observe(this, Observer {

        })
    }
}
