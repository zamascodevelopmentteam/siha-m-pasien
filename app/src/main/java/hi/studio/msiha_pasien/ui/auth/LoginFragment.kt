package hi.studio.msiha_pasien.ui.auth


import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.data.network.wrapper.Status
import hi.studio.msiha_pasien.ui.main.MainActivity
import hi.studio.msiha_pasien.util.extension.createViewModel
import hi.studio.msiha_pasien.util.extension.toast
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : Fragment() {

    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.getComponent().inject(this)
        viewModel = createViewModel { App.getComponent().getAuthViewModel() }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as? AppCompatActivity)?.supportActionBar?.hide()

        loginTextForgotPassword.setOnClickListener {
            findNavController().navigate(R.id.forgotPasswordFragment)
        }

        loginButton.setOnClickListener {
//            startActivity(Intent(context, MainActivity::class.java))
//            activity?.finish()
            if (isFormValid()) {
                viewModel.login(
                    loginInputNik.text?.trim().toString(),
                    loginInputPassword.text?.trim().toString()
                )
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.loginResult.observe(this, Observer { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    resource.message?.let { toast(it) }
                    handleLoading(false)
                    startActivity(Intent(context, MainActivity::class.java))
                    activity?.finish()
                }
                Status.ERROR -> {
                    resource.message?.let { toast(it) }
                    handleLoading(false)
                }
                Status.LOADING -> {
                    handleLoading(true)
                }
            }
        })
    }

    private fun isFormValid(): Boolean {
        if (TextUtils.isEmpty(loginInputNik.text)) {
            loginInputNik.error = getString(R.string.empty_field)
            return false
        }
        if (TextUtils.isEmpty(loginInputPassword.text)) {
            loginInputPassword.error = getString(R.string.empty_field)
            return false
        }
        return true
    }

    private fun handleLoading(isShowing: Boolean) {
        if (isShowing) {
            loginButton.visibility = View.GONE
            loginProgressBar.visibility = View.VISIBLE
        } else {
            loginButton.visibility = View.VISIBLE
            loginProgressBar.visibility = View.GONE
        }
    }
}
