package hi.studio.msiha_pasien.ui.main.medicine

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.data.model.RecipeGiven
import hi.studio.msiha_pasien.util.extension.orZero
import kotlinx.android.synthetic.main.item_medicine_take.view.*

class MedicineAdapter(private val interaction: Interaction) :
    RecyclerView.Adapter<MedicineAdapter.MedicineHolder>() {

    private var data: MutableList<RecipeGiven> = mutableListOf()
    private val itemLayout = R.layout.item_medicine_take

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedicineHolder {
        val inflater = LayoutInflater.from(parent.context)
        return MedicineHolder(inflater.inflate(itemLayout, parent, false))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: MedicineHolder, position: Int) {
        holder.bind(data[position], interaction)
    }

    fun swapData(data: List<RecipeGiven>) {
        this.data = data.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(data: List<RecipeGiven>) {
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    class MedicineHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(item: RecipeGiven, interaction: Interaction) = with(itemView) {
            itemMedicineTakeTextClinic.text = item.visit.hospital.name
            itemMedicineTakeTextAmount.text =
                item.recipe.recipeMedicines?.size.orZero().toString() + " Obat"

            setOnClickListener { interaction.onItemClick(item) }
        }
    }

    interface Interaction {
        fun onItemClick(item: RecipeGiven)
    }
}