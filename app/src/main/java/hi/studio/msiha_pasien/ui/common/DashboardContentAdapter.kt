package hi.studio.msiha_pasien.ui.common

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.data.model.ListRecipes
import kotlinx.android.synthetic.main.item_dashboard_content.view.*

class DashboardContentAdapter(private val interaction: Interaction) :
    RecyclerView.Adapter<DashboardContentAdapter.ContentHolder>() {

    private var data: MutableList<DashboardContentModel> = mutableListOf()
    private val itemLayout = R.layout.item_dashboard_content

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ContentHolder(
            inflater.inflate(
                itemLayout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ContentHolder, position: Int) {
        holder.bind(data[position], interaction)
    }

    fun swapData(data: List<DashboardContentModel>) {
        this.data = data.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(data: List<DashboardContentModel>) {
        this.data.addAll(data.toMutableList())
        notifyDataSetChanged()
    }

    class ContentHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(item: DashboardContentModel, interaction: Interaction) =
            with(itemView) {
                itemDashboardContentTitle.text = item.title
                itemDashBoardVisitDate.text = item.subtitle
                buttonVisitDetail.setOnClickListener { interaction.onItemClick(item) }
                setOnClickListener { interaction.onItemClick(item) }
            }
    }

    interface Interaction {
        fun onItemClick(item: DashboardContentModel)
    }
}