package hi.studio.msiha_pasien.ui.main.dashboard.schedule


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.data.model.VisitSchedule
import hi.studio.msiha_pasien.data.network.wrapper.Status
import hi.studio.msiha_pasien.util.AppsContant
import hi.studio.msiha_pasien.util.extension.createViewModel
import hi.studio.msiha_pasien.util.extension.orFalse
import hi.studio.msiha_pasien.util.extension.toast
import kotlinx.android.synthetic.main.content_empty_list.*
import kotlinx.android.synthetic.main.fragment_content.*

class ScheduleListFragment : Fragment(),
    ScheduleAdapter.Interaction {

    private val scheduleAdapter by lazy {
        ScheduleAdapter(this)
    }
    private val viewModel by lazy {
        createViewModel { App.getComponent().getListScheduleViewModel() }
    }

    private var nextPage = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.getComponent().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_content, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.getVisitSchedule(nextPage)
        contentRecycler.apply {
            adapter = scheduleAdapter
            layoutManager = LinearLayoutManager(context)
        }
        setupEmptyListMessage()
        observeViewModel()
    }

    private fun setupEmptyListMessage(){
        textEmptyMessage.text="Anda belum membuat jadwal pemeriksaan"
    }

    override fun onItemClick(item: VisitSchedule) {
        val bundle = Bundle().apply { putParcelable(AppsContant.BUNDLE_KEY, item) }
        findNavController().navigate(R.id.action_dashboardFragment_to_scheduleDetailFragment, bundle)
    }

    private fun observeViewModel() {
        viewModel.getVisitScheduleResult.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data?.isNotEmpty().orFalse()) {
                        nextPage++
                        scheduleAdapter.swapData(it.data.orEmpty())
                    }
                    if(scheduleAdapter.itemCount>0){
                        contentRecycler.visibility=View.VISIBLE
                        layoutEmptyState.visibility=View.INVISIBLE
                    }else{
                        contentRecycler.visibility=View.INVISIBLE
                        layoutEmptyState.visibility=View.VISIBLE
                    }
                }
                Status.ERROR -> toast(it.message)
                Status.LOADING -> {
                }
            }
        })
    }

    companion object {
        @JvmStatic
        fun newInstance() = ScheduleListFragment()
    }
}
