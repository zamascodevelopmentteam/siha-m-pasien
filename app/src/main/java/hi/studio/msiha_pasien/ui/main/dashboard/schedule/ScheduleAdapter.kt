package hi.studio.msiha_pasien.ui.main.dashboard.schedule

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.data.model.VisitSchedule
import hi.studio.msiha_pasien.util.extension.toStringDateTime
import kotlinx.android.synthetic.main.item_dashboard_content.view.*

class ScheduleAdapter(private val interaction: Interaction) :
    RecyclerView.Adapter<ScheduleAdapter.ScheduleHolder>() {

    private var data: MutableList<VisitSchedule> = mutableListOf()
    private val itemLayout = R.layout.item_dashboard_content

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScheduleHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ScheduleHolder(inflater.inflate(itemLayout, parent, false))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ScheduleHolder, position: Int) {
        holder.bind(data[position], interaction)
    }

    fun swapData(data: List<VisitSchedule>) {
        this.data = data.toMutableList()
        notifyDataSetChanged()
    }

    class ScheduleHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: VisitSchedule, interaction: Interaction) = with(itemView) {
            with(itemView) {
                itemDashboardContentTitle.text = "Pemeriksaan Kunjungan Ke-${item.visitOrdinal}"
                itemDashBoardVisitDate.text = item.visitDate.toStringDateTime()
                setOnClickListener { interaction.onItemClick(item) }
            }
        }
    }

    interface Interaction {
        fun onItemClick(item: VisitSchedule)
    }
}