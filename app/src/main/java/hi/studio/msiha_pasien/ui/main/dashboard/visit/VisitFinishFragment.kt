package hi.studio.msiha_pasien.ui.main.dashboard.visit


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController

import hi.studio.msiha_pasien.R
import kotlinx.android.synthetic.main.fragment_visit_finish.*

class VisitFinishFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as? AppCompatActivity)?.supportActionBar?.hide()
        return inflater.inflate(R.layout.fragment_visit_finish, container, false)
    }

    override fun onStop() {
        super.onStop()
        (activity as? AppCompatActivity)?.supportActionBar?.show()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        visitFinishButtonBack.setOnClickListener {
            findNavController().popBackStack(R.id.dashboardFragment, false)
        }
    }
}
