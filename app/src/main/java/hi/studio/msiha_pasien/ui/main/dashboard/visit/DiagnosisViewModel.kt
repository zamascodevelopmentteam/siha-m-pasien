package hi.studio.msiha_pasien.ui.main.dashboard.visit

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hi.studio.msiha_pasien.data.model.Hospital
import hi.studio.msiha_pasien.data.model.VisitByType
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.data.repo.HospitalRepo
import hi.studio.msiha_pasien.data.repo.VisitRepo
import hi.studio.msiha_pasien.util.toSingleEvent
import javax.inject.Inject

class DiagnosisViewModel @Inject constructor(
    private val visitRepo: VisitRepo,
    private val hospitalRepo: HospitalRepo
) : ViewModel() {

    val getVisitDetailResult =
        MutableLiveData<Resource<VisitByType>>().toSingleEvent()

    val getPreviousVisitDetailResult =
        MutableLiveData<Resource<VisitByType>>().toSingleEvent()

    val getHospitalByIdResult =
        MutableLiveData<Resource<Hospital>>().toSingleEvent()

    fun getVisitDetail(visitId: Int) {
        visitRepo.getVisitDetail(visitId) {
            getVisitDetailResult.postValue(it)
            it.data?.run {
                hospitalRepo.getHospitalById(hospitalId) { resHospital ->
                    getHospitalByIdResult.postValue(resHospital)
                }
            }
        }
    }

    fun getPreviousVisit(visitId:Int){
        visitRepo.getPreviousVisit(visitId){
            getPreviousVisitDetailResult.postValue(it)
//            it.data?.run {
//                hospitalRepo.getHospitalById(hospitalId) { resHospital ->
//                    getHospitalByIdResult.postValue(resHospital)
//                }
//            }
        }
    }
}