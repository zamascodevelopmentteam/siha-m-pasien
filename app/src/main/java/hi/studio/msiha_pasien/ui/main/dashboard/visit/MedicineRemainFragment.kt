package hi.studio.msiha_pasien.ui.main.dashboard.visit


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.data.model.MedicineGiven
import hi.studio.msiha_pasien.data.network.wrapper.Status
import hi.studio.msiha_pasien.util.extension.createViewModel
import hi.studio.msiha_pasien.util.extension.toast
import kotlinx.android.synthetic.main.fragment_medicine_remain.*

class MedicineRemainFragment : Fragment(), MedicineRemainAdapter.Interaction {

    private val viewModel by lazy {
        createViewModel { App.getComponent().getMedicineRemainViewModel() }
    }
    private lateinit var medicineRemainAdapter: MedicineRemainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.getComponent().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_medicine_remain, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        medicineRemainAdapter = MedicineRemainAdapter(this)
        medicineRemainRecycler.apply {
            adapter = medicineRemainAdapter
            layoutManager = LinearLayoutManager(context)
        }

        medicineRemainButtonSave.setOnClickListener {
            updatePatientMedicineAmount()
        }

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.run {
            listPatientMedicineResult.observe(this@MedicineRemainFragment, Observer {
                when (it.status) {
                    Status.SUCCESS -> medicineRemainAdapter.swapData(it.data.orEmpty())
                    Status.ERROR -> toast(it.message)
                    Status.LOADING -> {
                    }
                }
            })
            updatePatientMedicineAmountResult.observe(this@MedicineRemainFragment, Observer {
                when (it.status) {
                    Status.SUCCESS -> findNavController().navigate(R.id.qrFinishFragment)
                    Status.ERROR -> toast(it.message)
                    Status.LOADING -> {
                    }
                }
            })
        }
    }

    override fun onItemDecrement(item: MedicineGiven) {
        item.amount--
        medicineRemainAdapter.notifyDataSetChanged()
    }

    override fun onItemIncrement(item: MedicineGiven) {
        item.amount++
        medicineRemainAdapter.notifyDataSetChanged()
    }

    private fun updatePatientMedicineAmount() {
        viewModel.updatePatientMedicineAmount(medicineRemainAdapter.data)
    }
}
