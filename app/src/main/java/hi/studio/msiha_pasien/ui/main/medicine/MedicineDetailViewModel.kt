package hi.studio.msiha_pasien.ui.main.medicine

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hi.studio.msiha_pasien.data.model.Recipe
import hi.studio.msiha_pasien.data.model.RecipeGivenStatus
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.data.repo.RecipeRepo
import hi.studio.msiha_pasien.util.toSingleEvent
import javax.inject.Inject

class MedicineDetailViewModel @Inject constructor(private val repo: RecipeRepo) : ViewModel() {

    val detailRecipeResult = MutableLiveData<Resource<Recipe>>().toSingleEvent()

    fun getDetailRecipe(recipeId: Int) {
        repo.getRecipeDetail(recipeId) { detailRecipeResult.postValue(it) }
    }

    val recipeGivenStatusResult =
        MutableLiveData<Resource<RecipeGivenStatus>>().toSingleEvent()

    fun getRecipeGivenStatus() {
        repo.getRecipeGivenStatus { recipeGivenStatusResult.postValue(it) }
    }
}