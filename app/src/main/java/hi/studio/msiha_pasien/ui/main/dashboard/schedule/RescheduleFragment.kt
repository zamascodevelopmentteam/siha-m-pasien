package hi.studio.msiha_pasien.ui.main.dashboard.schedule


import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import hi.studio.msiha_pasien.R
import kotlinx.android.synthetic.main.fragment_reschedule.*
import java.util.*

class RescheduleFragment : Fragment(), DatePickerDialog.OnDateSetListener {

    private val datePickerDialog by lazy {
        val calendar = Calendar.getInstance()
        context?.let {
            DatePickerDialog(
                it,
                this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DATE)
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_reschedule, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rescheduleTextDate.setOnClickListener {
            datePickerDialog?.show()
        }

        rescheduleButtonSave.setOnClickListener {
            findNavController().popBackStack(R.id.dashboardFragment, false)
            findNavController().navigate(R.id.dashboardFragment)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onDateSet(datePicker: DatePicker?, year: Int, month: Int, date: Int) {
        rescheduleTextDate.text = "$date-$month-$year"
        context?.let {
            rescheduleButtonSave.background = ContextCompat.getDrawable(it, R.drawable.bg_round_blue)
            rescheduleButtonSave.setTextColor(ContextCompat.getColor(it, android.R.color.white))
            rescheduleButtonSave.isEnabled = true
        }
    }
}
