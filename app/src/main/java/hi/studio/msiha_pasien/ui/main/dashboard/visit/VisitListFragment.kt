package hi.studio.msiha_pasien.ui.main.dashboard.visit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.data.network.wrapper.Status
import hi.studio.msiha_pasien.ui.common.DashboardContentAdapter
import hi.studio.msiha_pasien.ui.common.DashboardContentModel
import hi.studio.msiha_pasien.util.AppsContant
import hi.studio.msiha_pasien.util.EndlessScrollListener
import hi.studio.msiha_pasien.util.extension.createViewModel
import hi.studio.msiha_pasien.util.extension.orFalse
import hi.studio.msiha_pasien.util.extension.toStringDateTime
import hi.studio.msiha_pasien.util.extension.toast
import kotlinx.android.synthetic.main.content_empty_list.*
import kotlinx.android.synthetic.main.fragment_content.*

class VisitListFragment : Fragment(),
    DashboardContentAdapter.Interaction {

    private val dashboardContentAdapter by lazy {
        DashboardContentAdapter(this)
    }
    private val viewModel by lazy {
        createViewModel { App.getComponent().getVisitListViewModel() }
    }

    private var nextPage = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.getComponent().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_content, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.getVisitByType(nextPage)
        contentRecycler.apply {
            adapter = dashboardContentAdapter
            layoutManager = LinearLayoutManager(context)
            addOnScrollListener(object : EndlessScrollListener() {
                override fun loadMore() {
                    viewModel.getVisitByType(nextPage)
                }
            })
        }
        setupEmptyListMessage()
        observeViewModel()
    }

    private fun setupEmptyListMessage() {
        imgEmpty.setImageDrawable(context?.let {
            ContextCompat.getDrawable(
                it,
                R.drawable.ic_empty_visit
            )
        })
        textEmptyMessage.text = "Tidak ada data kunjungan untuk ditampilkan"
    }

    override fun onItemClick(item: DashboardContentModel) {
        val bundle = Bundle().apply { putInt(AppsContant.BUNDLE_KEY, item.id) }
        findNavController().navigate(R.id.action_dashboardFragment_to_diagnosisFragment, bundle)
    }

    private fun observeViewModel() {
        viewModel.getVisitByTypeResult.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data?.isNotEmpty().orFalse()) {
                        contentRecycler.visibility = View.INVISIBLE
                        layoutEmptyState.visibility = View.VISIBLE
                        nextPage++
                        val contents = mutableListOf<DashboardContentModel>().apply {
                            for (item in it.data.orEmpty()) {
                                add(
                                    DashboardContentModel(
                                        item.id,
                                        "Kunjungan Ke-${item.ordinal}",
                                        item.visitDate//.toStringDateTime()
                                    )
                                )
                            }
                        }
                        dashboardContentAdapter.addData(contents)
                    }
                    if (dashboardContentAdapter.itemCount > 0) {
                        contentRecycler.visibility = View.VISIBLE
                        layoutEmptyState.visibility = View.INVISIBLE
                    } else {
                        contentRecycler.visibility = View.INVISIBLE
                        layoutEmptyState.visibility = View.VISIBLE
                    }
                }
                Status.ERROR -> toast(it.message)
                Status.LOADING -> {
                }
            }
        })
    }

    companion object {
        @JvmStatic
        fun newInstance() = VisitListFragment()
    }
}
