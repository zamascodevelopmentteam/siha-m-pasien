package hi.studio.msiha_pasien.ui.main

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI.setupWithNavController
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.util.FragmentInteraction
import hi.studio.msiha_pasien.util.UpdateChecker
import hi.studio.msiha_pasien.util.extension.toast
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), UpdateChecker.Callback {

    private var isOnPersonalThreadFragment = false
    private var mBackPressCounter = 0
    private val appBarConfig by lazy {
        AppBarConfiguration(
            setOf(
                R.id.dashboardFragment,
                R.id.medicineFragment,
                R.id.profileFragment
            )
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val updateChecker = UpdateChecker.init(this).setCallback(this).check()
        setupActionBarWithNavController(mainNavHostFragment.findNavController(), appBarConfig)
        setupBottomNav()
    }

    override fun onBackPressed() {
        if (isOnPersonalThreadFragment) {
            mBackPressCounter++
            if (mBackPressCounter == 2) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    finishAndRemoveTask()
                } else {
                    finish()
                }
            } else {
                toast(getString(R.string.msg_press_once_again_to_exit))
            }
        } else {
            val consumed = mainNavHostFragment.childFragmentManager.fragments.let {
                if (it.size > 0 && it[0] is FragmentInteraction) {
                    (it[0] as FragmentInteraction).onBackPressed()
                } else {
                    false
                }
            }

            val fragmentsSize = mainNavHostFragment.childFragmentManager.fragments.size

            mBackPressCounter = 0
            isOnPersonalThreadFragment = true

            if (!consumed && fragmentsSize > 1) {
                super.onBackPressed()
            } else {
                mainNavHostFragment.findNavController().navigateUp(appBarConfig)
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return mainNavHostFragment.findNavController().navigateUp(appBarConfig)
    }

    private fun setupBottomNav() {
        setupWithNavController(mainBottomNav, mainNavHostFragment.findNavController())
        mainNavHostFragment.findNavController().addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id == R.id.dashboardFragment
                || destination.id == R.id.medicineFragment
                || destination.id == R.id.profileFragment) {
                mainBottomNav.visibility = View.VISIBLE
            } else {
                mainBottomNav.visibility = View.GONE
            }
        }
    }


    override fun onStartUpdateCheck() {

    }

    override fun onStopUpdateCheck() {

    }

    override fun onUpdatePressed(url: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    override fun onSkipUpdatePressed() {

    }

    override fun onNoUpdate() {

    }

    override fun onCheckUpdateError(e: Throwable) {
        e.message?.let { toast(it) }
    }
}
