package hi.studio.msiha_pasien.ui.common

data class DashboardContentModel(
    val id: Int,
    val title: String,
    val subtitle: String
)