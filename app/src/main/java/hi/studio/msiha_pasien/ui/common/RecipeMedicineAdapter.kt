package hi.studio.msiha_pasien.ui.common

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.data.model.RecipeMedicine
import kotlinx.android.synthetic.main.item_recipe.view.*

class RecipeMedicineAdapter() : RecyclerView.Adapter<RecipeMedicineAdapter.RecipeHolder>() {

    private var data: List<RecipeMedicine> = mutableListOf()
    private val itemLayout = R.layout.item_recipe

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeHolder {
        val inflater = LayoutInflater.from(parent.context)
        return RecipeHolder(inflater.inflate(itemLayout, parent, false))
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: RecipeHolder, position: Int) {
        holder.bind(data[position])
    }

    fun swapData(data: List<RecipeMedicine>) {
        this.data = data
        notifyDataSetChanged()
    }

    class RecipeHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(item: RecipeMedicine) = with(itemView) {
            itemRecipeTextMedicineNameAndWeight.text = item.medicine.codeName
            itemRecipeTextMedicineAmount.text = "${item.amount} tablet"
            itemRecipeTextRule.text = item.rule
        }
    }
}