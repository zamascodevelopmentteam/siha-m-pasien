package hi.studio.msiha_pasien.ui.main.medicine


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.orhanobut.hawk.Hawk
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.data.model.RecipeGiven
import hi.studio.msiha_pasien.data.model.VisitByType
import hi.studio.msiha_pasien.data.network.wrapper.Status
import hi.studio.msiha_pasien.util.extension.createViewModel
import kotlinx.android.synthetic.main.content_empty_list.*
import kotlinx.android.synthetic.main.fragment_medicine.*

class MedicineListFragment : Fragment(), MedicineAdapter.Interaction {
    private var visit:VisitByType?=null
    private val viewModel by lazy {
        createViewModel { App.getComponent().getMedicineViewModel() }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.getComponent().inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_medicine, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        visit = Hawk.get<VisitByType>("current_visit")
        if(visit==null) {
            layoutEmptyState.visibility=View.VISIBLE
            medWrapper.visibility=View.INVISIBLE
            buttonUpdateMed.visibility=View.INVISIBLE
            setupEmptyListMessage()
        }else{
            buttonUpdateMed.visibility=View.VISIBLE
            layoutEmptyState.visibility=View.INVISIBLE
            medWrapper.visibility=View.VISIBLE
            populateMedicineLeftList()
        }

        buttonUpdateMed.setOnClickListener {
            visit?.treatment?.sisaObatArv1=editTextMedArv1.text.toString()
            visit?.treatment?.sisaObatArv2=editTextMedArv2.text.toString()
            visit?.treatment?.sisaObatArv3=editTextMedArv3.text.toString()
            progress.visibility=View.VISIBLE
            textButton.text=""
            //progress.visibility=View.VISIBLE
           // textButton.text=""A
            if(Hawk.contains("current_visit_id")) {
                val currentVisitId: Int = Hawk.get("current_visit_id")
                visit?.treatment?.let { it1 -> viewModel.updateSisaObat("$currentVisitId", it1) }
                observeViewModel()
            }
        }
    }

    private fun populateMedicineLeftList(){
        if(visit!=null){
            if(visit!!.treatment!=null){
                val treatment = visit!!.treatment
                Log.d("obatArv","ObatArv 1 ${treatment.obatArv1Data}")
                Log.d("obatArv","ObatArv 2 ${treatment.obatArv2Data}")
                Log.d("obatArv","ObatArv 3 ${treatment.obatArv3Data}")
                if(treatment.obatArv1Data!=null){
                    wrapperArv1.visibility=View.VISIBLE
                    obatArv1Name.text=treatment.obatArv1Data.name
                    editTextMedArv1.setText(treatment.sisaObatArv3?:"0")
                }else{
                    wrapperArv1.visibility=View.GONE
                }
                if(treatment.obatArv2Data!=null){
                    wrapperArv2.visibility=View.VISIBLE
                    obatArv2Name.text=treatment.obatArv2Data.name
                    editTextMedArv2.setText(treatment.sisaObatArv2?:"0")
                }else{
                    wrapperArv2.visibility=View.GONE
                }
                if(treatment.obatArv3Data!=null){
                    wrapperArv3.visibility=View.VISIBLE
                    obatArv3Name.text=treatment.obatArv3Data.name
                    editTextMedArv3.setText(treatment.sisaObatArv3?:"0")
                }else{
                    wrapperArv3.visibility=View.GONE
                }
            }
        }
    }

    private fun setupEmptyListMessage() {
        imgEmpty.setImageDrawable(context?.let {
            ContextCompat.getDrawable(
                it,
                R.drawable.ic_empty_recipe
            )
        })
        textEmptyMessage.text = "Belum ada obat yang dapat diambil"
    }

    override fun onItemClick(item: RecipeGiven) {
        val bundle = Bundle().apply {
            putInt("recipeId", item.recipe.id)
            putString("hospitalName", item.visit.hospital.name)
            putString("hospitalCity", "Kota Bandung")
        }
        findNavController().navigate(R.id.action_medicineFragment_to_medicineDetailFragment, bundle)
    }

    private fun observeViewModel() {
        viewModel.getUpdateSisaObat.observe(this, Observer {
            when(it.status){
                Status.SUCCESS-> {
                    Toast.makeText(context,"Berhasil mengubah sisa Obat",Toast.LENGTH_SHORT).show()
                    editTextMedArv1.setText("0")
                    editTextMedArv2.setText("0")
                    editTextMedArv3.setText("0")
                    populateMedicineLeftList()
                    progress.visibility=View.INVISIBLE
                    textButton.text="Ubah sisa obat"
                }
                Status.ERROR-> {
                    Toast.makeText(context,"Gagal mengubah sisa Obat",Toast.LENGTH_SHORT).show()

                    progress.visibility=View.INVISIBLE
                    textButton.text="Ubah sisa obat"
                }
                Status.LOADING->{

                }
            }
        })
    }
}
