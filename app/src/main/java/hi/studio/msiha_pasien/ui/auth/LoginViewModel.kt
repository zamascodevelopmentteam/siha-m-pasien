package hi.studio.msiha_pasien.ui.auth

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hi.studio.msiha_pasien.data.model.Login
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.data.repo.UserRepo
import hi.studio.msiha_pasien.util.toSingleEvent
import javax.inject.Inject

class LoginViewModel @Inject constructor(private val repo: UserRepo) : ViewModel() {

    val loginResult = MutableLiveData<Resource<Login>>().toSingleEvent()

    fun login(nik: String, password: String) {
        loginResult.postValue(Resource.loading())
        repo.login(nik, password) { loginResult.postValue(it) }
    }
}