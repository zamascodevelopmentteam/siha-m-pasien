package hi.studio.msiha_pasien.ui.main.dashboard.visit

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hi.studio.msiha_pasien.data.model.MedicineGiven
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.data.repo.MedicineRepo
import hi.studio.msiha_pasien.data.request.MedicinesGivenAmountRequest
import hi.studio.msiha_pasien.util.toSingleEvent
import javax.inject.Inject

class MedicineRemainViewModel @Inject constructor(private val repo: MedicineRepo) : ViewModel() {

    val listPatientMedicineResult =
        MutableLiveData<Resource<List<MedicineGiven>>>().toSingleEvent().apply {
            repo.listPatientMedicineAsync { postValue(it) }
        }

    val updatePatientMedicineAmountResult =
        MutableLiveData<Resource<Void>>().toSingleEvent()

    fun updatePatientMedicineAmount(medicinesGivens: List<MedicineGiven>) {
        val medicinesGivenAmountRequests = mutableListOf<MedicinesGivenAmountRequest>().apply {
            for (medicinesGiven in medicinesGivens) {
                add(MedicinesGivenAmountRequest(medicinesGiven.id, medicinesGiven.amount))
            }
        }
        repo.updatePatientMedicineAmount(medicinesGivenAmountRequests) {
            updatePatientMedicineAmountResult.postValue(it)
        }
    }
}