package hi.studio.msiha_pasien.ui.main.dashboard.visit

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.data.network.wrapper.Status
import hi.studio.msiha_pasien.util.extension.createViewModel
import hi.studio.msiha_pasien.util.extension.orFalse
import hi.studio.msiha_pasien.util.extension.textToImageEncode
import hi.studio.msiha_pasien.util.extension.toast
import kotlinx.android.synthetic.main.fragment_qr_start.*


class QrStartFragment : Fragment() {

    private lateinit var handler: Handler
    private lateinit var statusChecker: Runnable
    private var cancelTask = false
    private val viewModel by lazy {
        createViewModel { App.getComponent().getQrStartViewModel() }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.getComponent().inject(this)
        handler = Handler()
        statusChecker = Runnable {
            if (!cancelTask) {
                handler.postDelayed({
                    viewModel.getRecipeGivenStatus()
                    statusChecker.run()
                }, 1000L)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_qr_start, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showQrCode()
        observeViewModel()
        statusChecker.run()
    }

    override fun onPause() {
        super.onPause()
        cancelTask = true
        handler.removeCallbacks(statusChecker)
    }

    private fun showQrCode() {
        val user = App.instance.getUser()
        if (user == null) {
            toast("Tidak dapat menampilkan QR Code")
        } else {
            val image = user.nik.toString().textToImageEncode()
            qrStartImage.setImageBitmap(image)
        }
    }

    private fun observeViewModel() {
        viewModel.recipeGivenStatusResult.observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data?.isRecipeGiven.orFalse()) {
                        findNavController().navigate(R.id.medicineRemainFragment)
                    }
                }
                Status.ERROR -> {
                }
                Status.LOADING -> {
                }
            }
        })
    }
}