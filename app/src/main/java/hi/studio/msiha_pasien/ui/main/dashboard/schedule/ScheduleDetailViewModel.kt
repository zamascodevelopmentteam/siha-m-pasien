package hi.studio.msiha_pasien.ui.main.dashboard.schedule

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hi.studio.msiha_pasien.data.model.Hospital
import hi.studio.msiha_pasien.data.model.VisitByType
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.data.repo.HospitalRepo
import hi.studio.msiha_pasien.data.repo.VisitRepo
import hi.studio.msiha_pasien.util.toSingleEvent
import javax.inject.Inject

class ScheduleDetailViewModel @Inject constructor(private val repo: HospitalRepo) : ViewModel() {

    val getHospitalByIdResult =
        MutableLiveData<Resource<Hospital>>().toSingleEvent()

    fun getHospitalById(hospitalId: Int) {
        repo.getHospitalById(hospitalId) { resHospital ->
            getHospitalByIdResult.postValue(resHospital)
        }
    }
}