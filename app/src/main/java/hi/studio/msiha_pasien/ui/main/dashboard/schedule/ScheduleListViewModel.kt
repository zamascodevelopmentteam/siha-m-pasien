package hi.studio.msiha_pasien.ui.main.dashboard.schedule

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hi.studio.msiha_pasien.data.model.VisitSchedule
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.data.repo.VisitRepo
import hi.studio.msiha_pasien.util.toSingleEvent
import javax.inject.Inject

class ScheduleListViewModel @Inject constructor(private val repo: VisitRepo) : ViewModel() {

    val getVisitScheduleResult =
        MutableLiveData<Resource<List<VisitSchedule>>>().toSingleEvent()

    fun getVisitSchedule(page: Int) {
        repo.getVisitSchedule() {
            getVisitScheduleResult.postValue(it)
        }
    }
}