package hi.studio.msiha_pasien.ui.auth

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import hi.studio.msiha_pasien.util.UpdateChecker
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.R
import hi.studio.msiha_pasien.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : AppCompatActivity() {

    private val appBarConfig: AppBarConfiguration by lazy {
        AppBarConfiguration(setOf(R.id.loginFragment))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (App.instance.isLogin()) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        } else {
            setContentView(R.layout.activity_auth)
            setupActionBarWithNavController(authNavHostFragment.findNavController(), appBarConfig)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return authNavHostFragment.findNavController().navigateUp(appBarConfig)
    }

}
