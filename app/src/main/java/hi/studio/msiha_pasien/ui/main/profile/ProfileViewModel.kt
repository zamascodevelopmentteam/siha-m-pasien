package hi.studio.msiha_pasien.ui.main.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hi.studio.msiha_pasien.data.model.User
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.data.repo.UserRepo
import hi.studio.msiha_pasien.util.toSingleEvent
import javax.inject.Inject

class ProfileViewModel @Inject constructor(private val repo: UserRepo) : ViewModel() {

    val userResult = MutableLiveData<Resource<User>>().toSingleEvent().apply {
        repo.getUser { postValue(it) }
    }

    val logoutResult = MutableLiveData<Resource<Void>>().toSingleEvent()

    fun logout() {
        repo.logout { logoutResult.postValue(it) }
    }

}