package hi.studio.msiha_pasien.di

import javax.inject.Qualifier

@MustBeDocumented
@Qualifier
annotation class PrivateQualifier