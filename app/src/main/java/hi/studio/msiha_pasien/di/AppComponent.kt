package hi.studio.msiha_pasien.di

import dagger.Component
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.ui.auth.ForgotPasswordFragment
import hi.studio.msiha_pasien.ui.auth.ForgotPasswordViewModel
import hi.studio.msiha_pasien.ui.auth.LoginFragment
import hi.studio.msiha_pasien.ui.auth.LoginViewModel
import hi.studio.msiha_pasien.ui.main.dashboard.recipe.*
import hi.studio.msiha_pasien.ui.main.dashboard.schedule.ScheduleDetailFragment
import hi.studio.msiha_pasien.ui.main.dashboard.schedule.ScheduleDetailViewModel
import hi.studio.msiha_pasien.ui.main.dashboard.schedule.ScheduleListFragment
import hi.studio.msiha_pasien.ui.main.dashboard.schedule.ScheduleListViewModel
import hi.studio.msiha_pasien.ui.main.dashboard.visit.*
import hi.studio.msiha_pasien.ui.main.medicine.MedicineDetailFragment
import hi.studio.msiha_pasien.ui.main.medicine.MedicineDetailViewModel
import hi.studio.msiha_pasien.ui.main.medicine.MedicineListFragment
import hi.studio.msiha_pasien.ui.main.medicine.MedicineListViewModel
import hi.studio.msiha_pasien.ui.main.profile.ProfileFragment
import hi.studio.msiha_pasien.ui.main.profile.ProfileViewModel
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(app: App)

    fun inject(loginFragment: LoginFragment)
    fun getAuthViewModel(): LoginViewModel

    fun inject(profileFragment: ProfileFragment)
    fun getProfileViewModel(): ProfileViewModel

    fun inject(listRecipesFragment: RecipeListFragment)
    fun getListRecipesViewModel(): RecipeListViewModel

    fun inject(detailRecipeFragment: RecipeDetailFragment)
    fun getDetailRecipeViewModel(): RecipeDetailViewModel

    fun inject(qrStartFragment: QrStartFragment)
    fun getQrStartViewModel(): QrStartViewModel

    fun inject(medicineRemainFragment: MedicineRemainFragment)
    fun getMedicineRemainViewModel(): MedicineRemainViewModel

    fun inject(qrFinishFragment: QrFinishFragment)
    fun getQrFinishViewModel(): QrFinishViewModel

    fun inject(medicineFragment: MedicineListFragment)
    fun getMedicineViewModel(): MedicineListViewModel

    fun inject(medicineDetailFragment: MedicineDetailFragment)
    fun getMedicineDetailViewModel(): MedicineDetailViewModel

    fun inject(diagnosisFragment: DiagnosisFragment)
    fun getDiagnosisViewModel(): DiagnosisViewModel

    fun inject(listDiagnosisFragment: VisitListFragment)
    fun getVisitListViewModel(): VisitListViewModel

    fun inject(listScheduleFragment: ScheduleListFragment)
    fun getListScheduleViewModel(): ScheduleListViewModel

    fun inject(forgotPasswordFragment: ForgotPasswordFragment)
    fun getForgotPasswordViewModel(): ForgotPasswordViewModel

    fun getScheduleDetailViewModel(): ScheduleDetailViewModel
    fun inject(scheduleDetailFragment: ScheduleDetailFragment)
}