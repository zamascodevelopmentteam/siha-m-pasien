package hi.studio.msiha_pasien.di

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Module
import dagger.Provides
import hi.studio.msiha_pasien.BuildConfig
import hi.studio.msiha_pasien.data.network.token.TokenAuthenticator
import hi.studio.msiha_pasien.data.network.token.TokenInterceptor
import hi.studio.msiha_pasien.data.network.endpoint.PrivateEndpoint
import hi.studio.msiha_pasien.data.network.endpoint.PublicEndpoint
import hi.studio.msiha_pasien.util.AppsContant
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class AppModule(private val app: Application) {

    @Provides
    fun provideApp() = app

    @Provides
    fun provideAppContext(): Context = app.applicationContext

    @Provides
    @Singleton
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return loggingInterceptor
    }

    @Singleton
    @Provides
    fun provideGson(): Gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()

    @Provides
    @Singleton
    fun provideConverterFactory(gson: Gson): GsonConverterFactory =
        GsonConverterFactory.create(gson)

    @Provides
    @Singleton
    fun provideCallAdapterFactory() = CoroutineCallAdapterFactory()

    @Provides
    @Singleton
    @PrivateQualifier
    fun providePrivateClient(
        tokenInterceptor: TokenInterceptor,
        tokenAuthenticator: TokenAuthenticator,
        loggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(tokenInterceptor)
            .authenticator(tokenAuthenticator)
            //.addInterceptor(ChuckInterceptor(provideAppContext()))
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @Singleton
    @PublicQualifier
    fun providePublicClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(ChuckInterceptor(provideAppContext()))
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @Singleton
    @PrivateQualifier
    fun providePrivateRetrofit(
        @PrivateQualifier client: OkHttpClient,
        converterFactory: GsonConverterFactory,
        callAdapterFactory: CoroutineCallAdapterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(AppsContant.BASE_URL)
            .client(client)
            .addConverterFactory(converterFactory)
            .addCallAdapterFactory(callAdapterFactory)
            .build()
    }

    @Provides
    @Singleton
    @PublicQualifier
    fun providePublicRetrofit(
        @PublicQualifier client: OkHttpClient,
        converterFactory: GsonConverterFactory,
        callAdapterFactory: CoroutineCallAdapterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(AppsContant.BASE_URL)
            .client(client)
            .addConverterFactory(converterFactory)
            .addCallAdapterFactory(callAdapterFactory)
            .build()
    }

    @Provides
    @PrivateQualifier
    fun providePrivateEndpoint(@PrivateQualifier retrofit: Retrofit): PrivateEndpoint {
        return retrofit.create(PrivateEndpoint::class.java)
    }

    @Provides
    @PublicQualifier
    fun providePublicEndpoint(@PublicQualifier retrofit: Retrofit): PublicEndpoint {
        return retrofit.create(PublicEndpoint::class.java)
    }
}