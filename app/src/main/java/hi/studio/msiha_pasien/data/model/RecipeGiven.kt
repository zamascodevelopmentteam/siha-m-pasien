package hi.studio.msiha_pasien.data.model

import java.util.*

data class RecipeGiven(
    val id: Int,
    val createdAt: Date,
    val updatedAt: Date,
    val recipeId: Int,
    val visitId: Int,
    val recipe: Recipe,
    val visit: Visit
)