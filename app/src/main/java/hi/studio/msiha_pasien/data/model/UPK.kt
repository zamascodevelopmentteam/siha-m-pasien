package hi.studio.msiha_pasien.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class UPK(
    val name:String?
):Parcelable