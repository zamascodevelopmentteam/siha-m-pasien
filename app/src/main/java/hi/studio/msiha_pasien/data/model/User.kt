package hi.studio.msiha_pasien.data.model

import java.util.*

data class User(
    val id: Int,
    val nik: String,
    val regnas: String,
    val name: String,
    val role: String,
    val avatar: String,
    val updatedAt: Date,
    val createdAt: Date,
    val hospitalId: Int,
    val patient:Pasien
)