package hi.studio.msiha_pasien.data.model

data class RefreshToken(val token: String, val refreshToken: String)