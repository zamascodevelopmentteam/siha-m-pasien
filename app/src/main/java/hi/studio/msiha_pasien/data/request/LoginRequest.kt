package hi.studio.msiha_pasien.data.request

data class LoginRequest(
    val nik: String,
    val password: String,
    val fcmToken:String
)