package hi.studio.msiha_pasien.data.model

data class RecipeGivenStatus(val isRecipeGiven: Boolean)