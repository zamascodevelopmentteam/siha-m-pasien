package hi.studio.msiha_pasien.data.network.endpoint

import com.google.gson.JsonArray
import hi.studio.msiha_pasien.data.model.*
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface PrivateEndpoint {

    @GET("private/users/{userId}")
    fun getUserAsync(@Path("userId") userId: Int): Deferred<Response<Resource<User>>>

    @POST("private/auth/logout")
    fun logoutAsync(
        @Query("fcmToken") fcmToken:String
    ): Deferred<Response<Resource<Void>>>

    @GET("private/patient/{userId}/recipes")
    fun getRecipesAsync(
        @Path("userId") userId: Int,
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): Deferred<Response<Resource<List<ListRecipes>>>>

    @GET("private/recipes/{recipeId}")
    fun getRecipeDetailAsync(@Path("recipeId") recipeId: Int):
            Deferred<Response<Resource<Recipe>>>

    @GET("private/recipe-given-status")
    fun getRecipeGivenStatusAsync(): Deferred<Response<Resource<RecipeGivenStatus>>>

    @GET("/private/patient/{patientId}/medicines-given")
    fun listPatientMedicineAsync(@Path("patientId") patientId: Int):
            Deferred<Response<Resource<List<MedicineGiven>>>>

    @PUT("private/patient/{patientId}/medicines-given")
    fun updatePatientMedicineAmountAsync(
        @Path("patientId") patientId: Int,
        @Body request: JsonArray
    ): Deferred<Response<Resource<Void>>>

    @PUT("private/patient/{visitId}/medicines-given")
    fun updateSisaObat(
        @Path("visitId") visitId: Int,
        @Body request:Treatment
    ):Deferred<Response<Resource<Void>>>

    @GET("private/patient/{patientId}/recipes-given")
    fun getRecipesGivenAsync(
        @Path("patientId") patientId: Int,
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): Deferred<Response<Resource<List<RecipeGiven>>>>

    @GET("private/visit-status")
    fun getVisitStatusAsync(): Deferred<Response<Resource<VisitStatus>>>

    @GET("/private/patient/visit/TREATMENT")
    fun getVisitHivAsync(
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): Deferred<Response<Resource<List<VisitByType>>>>

    @GET("/private/visits/{visitId}")
    fun getVisitDetailAsync(
        @Path("visitId") visitId: Int
    ): Deferred<Response<Resource<VisitByType>>>

    @GET("private/hospitals/{hospitalId}")
    fun getHospitalByIdAsync(
        @Path("hospitalId") hospitalId: Int
    ): Deferred<Response<Resource<Hospital>>>

    @GET("private/patient/visit-schedule/")
    fun getVisitScheduleAsync(): Deferred<Response<Resource<List<VisitSchedule>>>>

    @GET("private/visit/{visitId}/previous-treatment")
    fun getPreviousVisit(
        @Path("visitId") visitId: Int
    ):Deferred<Response<Resource<VisitByType>>>
}