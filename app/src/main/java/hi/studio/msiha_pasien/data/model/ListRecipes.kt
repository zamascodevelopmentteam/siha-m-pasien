package hi.studio.msiha_pasien.data.model

import androidx.recyclerview.widget.DiffUtil
import com.google.gson.annotations.SerializedName


class ListRecipes(
    val id: Int,
    val recipeNumber: String,
    @SerializedName("medicine_total") val medicineTotal: Int
) {

    companion object {
        val DIFF_CALL: DiffUtil.ItemCallback<ListRecipes> = object :
            DiffUtil.ItemCallback<ListRecipes>() {

            override fun areItemsTheSame(
                oldItem: ListRecipes,
                newItem: ListRecipes
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: ListRecipes,
                newItem: ListRecipes
            ): Boolean {
                return oldItem.id == newItem.id
            }

        }
    }

}