package hi.studio.msiha_pasien.data.network.wrapper

data class Paging(val page: Int, val size: Int, val total: Int)