package hi.studio.msiha_pasien.data.model

data class VisitStatus(val status: Boolean)