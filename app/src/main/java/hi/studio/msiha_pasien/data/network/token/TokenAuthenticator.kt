package hi.studio.msiha_pasien.data.network.token

import android.util.Log
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.data.request.RefreshTokenRequest
import hi.studio.msiha_pasien.data.network.endpoint.PublicEndpoint
import hi.studio.msiha_pasien.di.PublicQualifier
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import javax.inject.Inject

class TokenAuthenticator @Inject constructor(@PublicQualifier private val endpoint: PublicEndpoint) :
    Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {
        if (response.code() == 401) {
            val userId: Int = App.instance.getUser()?.id ?: -1
            var refreshToken: String = App.instance.getRefreshToken().orEmpty()
            val request = RefreshTokenRequest(userId, refreshToken)
            val call = endpoint.refreshToken(request)
            var accessToken: String? = null

            try {
                val responseCall = call.execute()
                val responseRequest = responseCall.body()
                if (responseRequest != null) {
                    val data = responseRequest.data
                    accessToken = data?.token.orEmpty()
                    refreshToken = data?.refreshToken.orEmpty()
                    App.instance.setToken(accessToken)
                    App.instance.setRefreshToken(refreshToken)
                }
            } catch (exception: Exception) {
                Log.d("ERROR", "response: $exception")
            }

            return if (accessToken != null) {
                response.request().newBuilder()
                    .header("Authorization", "Bearer $accessToken")
                    .build()
            } else {
                null
            }
        }

        return null
    }
}
