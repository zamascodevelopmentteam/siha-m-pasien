package hi.studio.msiha_pasien.data.request

data class RefreshTokenRequest(val userId: Int, val refreshToken: String)