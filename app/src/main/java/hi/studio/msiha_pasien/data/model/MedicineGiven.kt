package hi.studio.msiha_pasien.data.model

import java.util.*

data class MedicineGiven(
    val id: Int,
    var amount: Int,
    val createdAt: Date,
    val updatedAt: Date,
    val medicine: Medicine
)