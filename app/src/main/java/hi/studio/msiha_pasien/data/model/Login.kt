package hi.studio.msiha_pasien.data.model

data class Login(
    val token: String,
    val refreshToken: String,
    val user: User
)