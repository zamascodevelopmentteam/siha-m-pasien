package hi.studio.msiha_pasien.data.model

class Pasien(
     val id:String?,
     val nik:String?,
     val fullname:String?,
     val addressKTP:String?,
     val addressDomicile:String?,
     val dateBirth:String?,
     val gender:String?,
     val phone:String?,
     val statusPatient:String?,
     val weight:String?,
     val height:String?,
     val namaPmo:String?,
     val hubunganPmo:String?,
     val noHpPmo:String?,
     val createdBy:String?,
     val udpatedBy:String?,
     val upkId:String?,
     val createdAt:String?,
     val updatedAt:String?,
     val deletedAt:String?,
     val UserId:String?
)