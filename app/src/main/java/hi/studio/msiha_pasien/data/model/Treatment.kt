package hi.studio.msiha_pasien.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Treatment(
    val id:String?,
    val ordinal:String?,
    val isGenerated:String?,
    val treatmentStartDate:String?,
    val noRegNas:String?,
    val tglKonfirmasiHivPos:String?,
    val tglKunjungan:String?,
    val tglRujukMasuk:String?,
    val upkSebelumnya:String?,
    val kelompokPopulasi:String,
    val statusTb:String?,
    val tglPengobatanTb:String?,
    val statusFungsional:String?,
    val stadiumKlinis:String?,
    val ppk:String,
    val statusTbTpt:String?,
    var sisaObatArv1: String?,
    var sisaObatArv2: String?,
    var sisaObatArv3: String?,
    val tglPemberianObat:String?,
    val tglRencanaKunjungan:String?,
    val obatArv1Data:ObatArv?,
    val obatArv2Data:ObatArv?,
    val obatArv3Data:ObatArv?,
    val jmlHrObatArv1:Int?,
    val jmlHrObatArv2:Int?,
    val jmlHrObatArv3:Int?
):Parcelable