package hi.studio.msiha_pasien.data.network.endpoint

import hi.studio.msiha_pasien.data.model.Login
import hi.studio.msiha_pasien.data.model.RefreshToken
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.data.request.LoginRequest
import hi.studio.msiha_pasien.data.request.RefreshTokenRequest
import hi.studio.msiha_pasien.data.request.ResetPasswordRequest
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface PublicEndpoint {

    @POST("public/auth/login")
    fun loginAsync(@Body loginRequest: LoginRequest): Deferred<Response<Resource<Login>>>


    @POST("public/auth/refresh")
    fun refreshToken(@Body refreshTokenRequest: RefreshTokenRequest): Call<Resource<RefreshToken>>

    @POST("public/auth/reset")
    fun resetPassword(@Body resetPasswordRequest: ResetPasswordRequest): Deferred<Response<Resource<Void>>>

}