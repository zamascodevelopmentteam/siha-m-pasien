package hi.studio.msiha_pasien.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class VisitSchedule(
    val id: Int,
    val visitOrdinal: Int,
    val visitType: String,
    val visitDate: Long,
    val doctorName: String,
    val createdAt: Date,
    val updatedAt: Date,
    @SerializedName("UserId") val userId: Int,
    val hospitalId: Int
) : Parcelable