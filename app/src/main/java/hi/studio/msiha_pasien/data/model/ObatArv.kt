package hi.studio.msiha_pasien.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class ObatArv (
    val id:String,
    val name:String?,
    val codeName:String?,
    val stockUnitType:String?,
    val packageUnitType:String?,
    val packageMultiplier:String?,
    val ingredients:String?,
    val createdAt:String?,
    val updatedAt:String,
    val deletedAt:String
):Parcelable