package hi.studio.msiha_pasien.data.model

import java.util.*

data class RecipeMedicine(
    val id: Int,
    val rule: String,
    val amount: Int,
    val createdAt: Date,
    val updatedAt: Date,
    val medicineId: Int,
    val recipeId: Int,
    val medicine: Medicine
)