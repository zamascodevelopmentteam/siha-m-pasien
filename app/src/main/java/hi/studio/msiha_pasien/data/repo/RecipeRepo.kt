package hi.studio.msiha_pasien.data.repo

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.data.model.*
import hi.studio.msiha_pasien.data.network.endpoint.PrivateEndpoint
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.data.network.wrapper.Status
import hi.studio.msiha_pasien.data.request.MedicinesGivenAmountRequest
import hi.studio.msiha_pasien.di.PrivateQualifier
import hi.studio.msiha_pasien.util.extension.coroutineLaunch
import hi.studio.msiha_pasien.util.extension.orInvalidId
import hi.studio.msiha_pasien.util.extension.parse
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class RecipeRepo @Inject constructor(@PrivateQualifier private val endpoint: PrivateEndpoint) {

    fun getAllRecipes(
        page: Int,
        limit: Int = 10,
        callback: (Resource<List<ListRecipes>>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            try {
                endpoint.getRecipesAsync(App.instance.getUser()?.id.orInvalidId(), page, limit)
                    .await()
                    .parse(callback)
            } catch (e: Throwable) {
                callback(Resource.error("Tidak dapat terhubung ke server"))
            }
        }
    }

    fun getRecipeDetail(recipeId: Int, callback: (Resource<Recipe>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            try {
                endpoint.getRecipeDetailAsync(recipeId).await().parse(callback)
            } catch (e: Throwable) {
                callback(Resource.error("Tidak dapat terhubung ke server"))
            }
        }
    }

    fun getRecipeGivenStatus(callback: (Resource<RecipeGivenStatus>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            try {
                endpoint.getRecipeGivenStatusAsync().await().parse(callback)
            } catch (e: Throwable) {
                callback(Resource.error("Tidak dapat terhubung ke server"))
            }
        }
    }

    fun getRecipesGiven(
        page: Int,
        limit: Int = 10,
        callback: (Resource<List<RecipeGiven>>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            try {
                endpoint.getRecipesGivenAsync(App.instance.getUser()?.id.orInvalidId(), page, limit)
                    .await().parse(callback)
            } catch (e: Throwable) {
                callback(Resource.error("Tidak dapat terhubung ke server"))
            }
        }
    }

    fun updateSisaObat(
        id:String,
        request: Treatment,
        callback: (Resource<Void>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            try {

                endpoint.updateSisaObat(
                    id.toInt(),
                    request
                    ).await().parse {callback(it)}
            } catch (e: Throwable) {
                callback(Resource.error("Tidak dapat terhubung ke server"))
            }
        }
    }
}