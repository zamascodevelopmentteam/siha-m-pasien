package hi.studio.msiha_pasien.data.request

class MedicinesGivenAmountRequest(val id: Int, val amount: Int)