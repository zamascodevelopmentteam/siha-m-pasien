package hi.studio.msiha_pasien.data.model

import java.util.*

data class Medicine(
    val id: Int,
    val name: String,
    val codeName: String?,
    val type: String?,
    val picture: String?,
    val medicineType: String,
    val ingredients: String?,
    val usage: String?,
    val createdAt: Date?,
    val updatedAt: Date?
)