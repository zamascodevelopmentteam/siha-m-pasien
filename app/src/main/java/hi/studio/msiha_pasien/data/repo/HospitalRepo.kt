package hi.studio.msiha_pasien.data.repo

import hi.studio.msiha_pasien.data.model.Hospital
import hi.studio.msiha_pasien.data.network.endpoint.PrivateEndpoint
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.di.PrivateQualifier
import hi.studio.msiha_pasien.util.extension.coroutineLaunch
import hi.studio.msiha_pasien.util.extension.parse
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class HospitalRepo @Inject constructor(@PrivateQualifier private val endpoint: PrivateEndpoint) {

    fun getHospitalById(
        hospitalId: Int,
        callback: (Resource<Hospital>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            try {
                endpoint.getHospitalByIdAsync(hospitalId).await().parse(callback)
            } catch (e: Throwable) {
                callback(Resource.error("Tidak dapat terhubung ke server"))
            }
        }
    }
}