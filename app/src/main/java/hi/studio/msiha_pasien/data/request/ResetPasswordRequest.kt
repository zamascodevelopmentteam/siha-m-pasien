package hi.studio.msiha_pasien.data.request

data class ResetPasswordRequest(val nik: String)