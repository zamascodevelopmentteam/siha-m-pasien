package hi.studio.msiha_pasien.data.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class Recipe(
    val id: Int,
    val recipeNumber: String,
    val notes: String,
    val givenAt: Long,
    val createdAt: Date,
    val updatedAt: Date,
    @SerializedName("UserId") val userId: Int,
    val recipeMedicines: List<RecipeMedicine>?
)