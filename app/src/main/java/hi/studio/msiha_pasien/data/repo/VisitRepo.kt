package hi.studio.msiha_pasien.data.repo

import android.util.Log
import hi.studio.msiha_pasien.data.model.VisitByType
import hi.studio.msiha_pasien.data.model.VisitSchedule
import hi.studio.msiha_pasien.data.model.VisitStatus
import hi.studio.msiha_pasien.data.network.endpoint.PrivateEndpoint
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.di.PrivateQualifier
import hi.studio.msiha_pasien.util.extension.coroutineLaunch
import hi.studio.msiha_pasien.util.extension.parse
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class VisitRepo @Inject constructor(@PrivateQualifier private val endpoint: PrivateEndpoint) {

    fun getVisitStatus(callback: (Resource<VisitStatus>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            try {
                endpoint.getVisitStatusAsync().await().parse(callback)
            } catch (e: Throwable) {
                callback(Resource.error("Tidak dapat terhubung ke server"))
            }

        }
    }

    fun getVisitByType(page: Int, callback: (Resource<List<VisitByType>>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            try {
                endpoint.getVisitHivAsync(page, 10).await().parse(callback)
            } catch (e: Throwable) {
                callback(Resource.error("Tidak dapat terhubung ke server"))
            }
        }
    }

    fun getVisitDetail(
        visitId: Int,
        callback: (Resource<VisitByType>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            try {
                endpoint.getVisitDetailAsync(visitId).await().parse(callback)
            } catch (e: Throwable) {
                callback(Resource.error("Tidak dapat terhubung ke server"))
            }
        }
    }

    fun getPreviousVisit(
        visitId: Int,
        callback: (Resource<VisitByType>) -> Unit
    ){
        coroutineLaunch(Dispatchers.IO) {
            try{
                endpoint.getPreviousVisit(visitId).await().parse(callback)
            }catch (e:Throwable){
                callback(Resource.error("Tidak dapat terhubung ke server"))
            }
        }
    }

    fun getVisitSchedule(callback: (Resource<List<VisitSchedule>>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            try {
                endpoint.getVisitScheduleAsync().await().parse(callback)
            } catch (e: Throwable) {
                callback(Resource.error("Tidak dapat terhubung ke server"))
            }
        }
    }
}