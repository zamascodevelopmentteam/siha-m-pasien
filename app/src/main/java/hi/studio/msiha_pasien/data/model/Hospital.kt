package hi.studio.msiha_pasien.data.model

import java.util.*

data class Hospital(
    val id: Int,
    val name: String,
    val createdAt: Date,
    val updatedAt: Date
)