package hi.studio.msiha_pasien.data.model

import com.google.gson.annotations.SerializedName

data class Visit(
    @SerializedName("hospital_id") val hospitalId: Int,
    val hospital: Hospital
)