package hi.studio.msiha_pasien.data.repo

import com.orhanobut.hawk.Hawk
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.data.model.Login
import hi.studio.msiha_pasien.data.model.User
import hi.studio.msiha_pasien.data.network.endpoint.PrivateEndpoint
import hi.studio.msiha_pasien.data.network.endpoint.PublicEndpoint
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.data.network.wrapper.Status
import hi.studio.msiha_pasien.data.request.LoginRequest
import hi.studio.msiha_pasien.data.request.ResetPasswordRequest
import hi.studio.msiha_pasien.di.PrivateQualifier
import hi.studio.msiha_pasien.di.PublicQualifier
import hi.studio.msiha_pasien.util.AppsContant.KEY_FCM_TOKEN
import hi.studio.msiha_pasien.util.extension.coroutineLaunch
import hi.studio.msiha_pasien.util.extension.parse
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class UserRepo @Inject constructor(
    @PrivateQualifier private val privateEndpoint: PrivateEndpoint,
    @PublicQualifier private val publicEndpoint: PublicEndpoint
) {

    fun login(nik: String, password: String, callback: (Resource<Login>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            try {
                publicEndpoint.loginAsync(
                    LoginRequest(
                        nik,
                        password,
                        Hawk.get(KEY_FCM_TOKEN)?:""
                    )
                ).await().parse { resource ->
                    resource.data?.let {
                        App.instance.setUser(it.user)
                        App.instance.setToken(it.token)
                        App.instance.setRefreshToken(it.refreshToken)
                    }
                    callback(resource)
                }
            } catch (e: Throwable) {
                callback(Resource.error("Tidak dapat terhubung ke server"))
            }
        }
    }

    fun getUser(callback: (Resource<User>) -> Unit) {
        val user = App.instance.getUser()

        user?.let {
            coroutineLaunch(Dispatchers.IO) {
                try {
                    privateEndpoint.getUserAsync(it.id).await().parse(callback)
                } catch (e: Throwable) {
                    callback(Resource.error("Tidak dapat terhubung ke server"))
                }
            }
        }
    }

    fun logout(callback: (Resource<Void>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            try {
                privateEndpoint.logoutAsync(Hawk.get(KEY_FCM_TOKEN)?:"").await().parse {
                    if (it.status == Status.SUCCESS) {
                        App.instance.logout()
                    }
                    callback(it)
                }
            } catch (e: Throwable) {
                callback(Resource.error("Tidak dapat terhubung ke server"))
            }
        }
    }

    fun resetPassword(
        resetPasswordRequest: ResetPasswordRequest,
        callback: (Resource<Void>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            try {
                publicEndpoint.resetPassword(resetPasswordRequest).await().parse(callback)
            } catch (e: Throwable) {
                callback(Resource.error("Tidak dapat terhubung ke server"))
            }
        }
    }
}