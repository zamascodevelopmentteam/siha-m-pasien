package hi.studio.msiha_pasien.data.repo

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import hi.studio.msiha_pasien.App
import hi.studio.msiha_pasien.data.model.MedicineGiven
import hi.studio.msiha_pasien.data.network.endpoint.PrivateEndpoint
import hi.studio.msiha_pasien.data.network.wrapper.Resource
import hi.studio.msiha_pasien.data.request.MedicinesGivenAmountRequest
import hi.studio.msiha_pasien.di.PrivateQualifier
import hi.studio.msiha_pasien.util.extension.coroutineLaunch
import hi.studio.msiha_pasien.util.extension.orInvalidId
import hi.studio.msiha_pasien.util.extension.parse
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class MedicineRepo @Inject constructor(@PrivateQualifier private val endpoint: PrivateEndpoint) {

    fun listPatientMedicineAsync(callback: (Resource<List<MedicineGiven>>) -> Unit) {
        coroutineLaunch(Dispatchers.IO) {
            try {
                endpoint.listPatientMedicineAsync(App.instance.getUser()?.id.orInvalidId()).await()
                    .parse(callback)
            } catch (e: Throwable) {
                callback(Resource.error("Tidak dapat terhubung ke server"))
            }
        }
    }

    fun updatePatientMedicineAmount(
        request: List<MedicinesGivenAmountRequest>,
        callback: (Resource<Void>) -> Unit
    ) {
        coroutineLaunch(Dispatchers.IO) {
            try {
                val data = JsonArray()
                for (item in request) {
                    val itemJson = JsonObject()
                    itemJson.addProperty("id", item.id)
                    itemJson.addProperty("amount", item.amount)
                    data.add(itemJson)
                }
                endpoint.updatePatientMedicineAmountAsync(
                    App.instance.getUser()?.id.orInvalidId(),
                    data
                ).await().parse(callback)
            } catch (e: Throwable) {
                callback(Resource.error("Tidak dapat terhubung ke server"))
            }
        }
    }
}