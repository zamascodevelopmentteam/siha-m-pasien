package hi.studio.msiha_pasien.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class VisitByType(
    val id: Int,
    val ordinal: Int,
    val weight: Int?,
    val height: Int?,
    val functionalStatus: String?,
    val clinicalStadium: String?,
    val pregnant: String?,
    val kbMethod: String?,
    val infectionOpotunities: String?,
    val tbStatus: String?,
    val treatmentMedicine: String?,
    val treatmentResult: String?,
    val arvMedicineLeft: String?,
    val labResult: String?,
    val condomGiven: String?,
    val condomAmount: String?,
    val followUp: String?,
    val visitType: String,
    val visitDate: String,
    val upk: UPK?,
    val checkOutDate:String?,
    val userRrId:String?,
    val patientId:String,
    val createdAt: Date,
    val updatedAt: Date,
    @SerializedName("UserId") val userId: Int,
    val hospitalId: Int,
    val treatment: Treatment
):Parcelable