package hi.studio.msiha_pasien.data.network.token

import hi.studio.msiha_pasien.App
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class TokenInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val token = App.instance.getToken()
        val original = chain.request()
        val builder = original.newBuilder()
        if (token != null) {
            builder.header("Authorization", "Bearer $token")
        }
        val request = builder.build()
        return chain.proceed(request)
    }
}